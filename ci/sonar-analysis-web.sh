#!/usr/bin/env sh

set -e

cd web/$1
npm i
npm run sonarqube