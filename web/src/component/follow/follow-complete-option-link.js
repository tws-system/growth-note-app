import React, {Component} from 'react'
import {connect} from 'react-redux'
import {FormattedMessage} from 'react-intl'
import * as follow from '../../action/follow'

class FollowOptionLink extends Component {
  render () {
    const {item} = this.props
    const actionStr = item.followed ? <span style={{float: 'right', color: '#C0C0C0'}}><FormattedMessage id='已关注' /></span>
      : <span style={{float: 'right'}}><FormattedMessage id='未关注' /></span>
    return actionStr
  }
}

const mapStateToProps = state => ({contacts: state.diariesAndContactInfoList, contactUsers: state.contactUsers})

const mapDispatchToProps = dispatch => ({
  getContactList: () => dispatch(follow.getFolloweeListAndDiaries()),
  search: (value) => dispatch(follow.search(value)),
  getContactDiariesAndComs: (contactUserId) => dispatch(follow.getFolloweeDiariesAndComments(contactUserId))
})

export default connect(mapStateToProps, mapDispatchToProps)(FollowOptionLink)
