import React, {Component} from 'react'
import {Card} from 'antd'
import {connect} from 'react-redux'
import * as actions from '../../action/practise-diary'
import 'antd/dist/antd.css'
import {FormattedMessage} from 'react-intl'
import PractiseDiaryEditorBody from './practise-diary-editor-body'
import { withRouter } from 'react-router-dom'

class PractiseDiaryEditor extends Component {
  render () {
    return (<div className='practise-diary'>
      <Card title={<FormattedMessage id='新的日志' />}
        bordered className='title-text-position' noHovering>
          <PractiseDiaryEditorBody createPractiseDiary={this.props.submitPractiseDiary} operationType='create' />
      </Card>
    </div>)
  }
}

const mapDispatchToProps = dispatch => ({
  submitPractiseDiary: practiseDiary => dispatch(actions.createPractiseDiary(practiseDiary))
})

export default withRouter(connect(() => {
  return {}
}, mapDispatchToProps)(PractiseDiaryEditor))
