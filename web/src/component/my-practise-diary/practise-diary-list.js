import React, { Component } from 'react'
import { connect } from 'react-redux'
import {FormattedMessage} from 'react-intl'
import * as actions from '../../action/practise-diary'
import PractiseDiary from './practise-diary'
import PractiseDiaryEditor from './practise-diary-editor'
import constant from '../../constant/constant'
import 'antd/dist/antd.css'
import { Pagination, Row, Col, Button } from 'antd'
import * as followActions from '../../action/follow'
import moment from 'moment'
import DiaryListMonthView from '../common/diary-list-month-view'
import { withRouter } from 'react-router-dom'

class PractiseDiaryList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      page: 1,
      pageSize: constant.PAGE_SIZE,
      isShowMonthView: false
    }
  }

  componentDidMount () {
    this.props.getPractiseDiaries(this.state.page, this.state.pageSize)
    this.props.getFollowTutors()
  }

  onChange (page) {
    this.setState({ page })
    this.props.getPractiseDiaries(page, this.state.pageSize)
  }
  showAndCloseMonthView () {
    this.setState({
      isShowMonthView: !this.state.isShowMonthView
    })
    const currentMonth = moment().month()
    const currentYear = moment().year()
    this.props.getMonthlyContactDiaries(currentMonth, currentYear)
    this.props.getYearSubmitCount(currentYear)
  }
  onMonthChange (month, year) {
    this.props.getMonthlyContactDiaries(month, year)
    this.props.getYearSubmitCount(year)
  }

  render () {
    const practiseDiaryAndComments = this.props.practiseDiariesInfo.practiseDiaryAndComments || []
    const practiseDiaryListView =
      <div>
        <PractiseDiaryEditor />
        {practiseDiaryAndComments.map((practiseDiaryAndComment, index) =>
          <PractiseDiary
            page={this.state.page} pageSise={this.state.pageSize}
            id={practiseDiaryAndComment.practiseDiary.id}
            key={index}
          />)}
            <Pagination className='practise-diary'
              style={{ 'display': this.props.practiseDiariesInfo.total ? '' : 'none' }}
              onChange={this.onChange.bind(this)}
              defaultCurrent={this.state.page} total={this.props.practiseDiariesInfo.total}
              defaultPageSize={constant.PAGE_SIZE} />
      </div>
    return (
      <div>
        <Row type='flex' justify='end' gutter={{ xs: 8, sm: 16, md: 24 }}>
          <Col>
            <Button
              type='primary'
              size='small'
              ghost
              className='button-note'
              onClick={() => this.showAndCloseMonthView()} >
              {this.state.isShowMonthView ? <FormattedMessage id='列表视图' /> : <FormattedMessage id='月视图' />}
            </Button>
          </Col>
        </Row>
          <Row>&nbsp;</Row>

        {
          !this.state.isShowMonthView
            ? practiseDiaryListView
            : <DiaryListMonthView
              monthlyDiaries={[...this.props.monthlyDiaries]}
              yearSubmitCount={[...this.props.yearSubmitCount]}
              onMonthChange={(month, year) => this.onMonthChange(month, year)}
            />
        }
      </div>
    )
  }
}

const mapStateToProps = state => (
  {
    practiseDiariesInfo: state.practiseDiariesInfo,
    monthlyDiaries: state.monthlyDiaries,
    yearSubmitCount: state.yearSubmitCount
  }
)
const mapDispatchToProps = dispatch => ({
  getPractiseDiaries: (page, pageSize) => dispatch(actions.getPractiseDiary(page, pageSize)),
  getFollowTutors: () => { dispatch(followActions.getFollowTutors()) },
  getMonthlyContactDiaries: (month, year) => dispatch(actions.getMonthlyDiaries(month, year)),
  getYearSubmitCount: (year) => dispatch(actions.getYearSubmitCount(year))

})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PractiseDiaryList))
