import React, { Component } from 'react'
import PractiseDiaryList from './my-practise-diary/practise-diary-list'
import ContactDiary from './follow/followee-diary-list'
import ExcellentPractiseDiary from './excellent-diary/excellent-diary-list'
import '../style/App.css'
import ContactList from './follow/followee-list'
import {connect} from 'react-redux'
import TwsLayout from './tws-layout'
import { HashRouter as Router, Route } from 'react-router-dom'
import { LocaleProvider } from 'antd'
import { IntlProvider } from 'react-intl'
import locales from '../locales'
import {getLangFromLocalStorage} from '../constant/lang-util'

class App extends Component {
  render () {
    const lang = this.props.lang || getLangFromLocalStorage()
    const langData = locales[lang]

    return (
      <IntlProvider locale={langData.intlLocale} messages={langData.intlMessage}>
        <LocaleProvider locale={langData.locale}>
          <Router>
            <TwsLayout>
              <Route exact path='/' component={PractiseDiaryList} />
                <Route exact path='/practise-diaries' component={PractiseDiaryList} />
                <Route exact path='/followees' component={ContactList} />
                <Route exact path='/followees/:id' component={ContactDiary} />
                <Route exact path='/excellent-diaries' component={ExcellentPractiseDiary} />
            </TwsLayout>
          </Router>
        </LocaleProvider>
      </IntlProvider>
    )
  }
}

const mapStateToProps = ({lang}) => ({lang})

export default connect(mapStateToProps)(App)
