import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Layout } from 'antd'
import LeftMenu from './left-menu'
import logo from '../../images/tw-logo-white-new.png'
import '../../less/tws-layout.less'
import * as notificationActions from '../../action/notification'
import LanguageButtons from './language-buttons'
import { TwsLayout } from 'tws-antd'
import BreadCrumbs from './tws-bread-crumb'

const {Content, Sider} = Layout

class Index extends Component {
  inIframe = () => {
    return window.self !== window.top
  }

  render () {
    const {user, settings, notifications, lang} = this.props
    const {userCenterHomeUrl, logoutUrl, notificationMoreUrl} = settings
    if (this.inIframe()) {
      return <Content >
        <div style={{padding: '0 20px', background: '#f0f2f5'}}>
          <BreadCrumbs settings={settings} />
        </div>
          <Layout style={{ background: '#fff'}}>
            <Sider width={200}>
              <LeftMenu />
            </Sider>
              <Content style={{padding: '20'}}>
                {this.props.children}
              </Content>
          </Layout>
      </Content>
    }
    return (
      <TwsLayout
        user={user}
        logo={logo}
        lang={lang}
        languageButtons={<LanguageButtons />}
        twsBreadcrumb={<BreadCrumbs />}
        logoutUrl={logoutUrl}
        userCenterHomeUrl={userCenterHomeUrl}
        moreUrl={notificationMoreUrl}
        notifications={notifications}
        handleOnClick={this.props.updateUnReadToReadById}
      >
        <Layout style={{ background: '#fff'}}>
          <Sider width={200}>
            <LeftMenu />
          </Sider>
            <Content style={{padding: '0 12px'}}>
              {this.props.children}
            </Content>
        </Layout>
      </TwsLayout>
    )
  }
}

const mapStateToProps = state => ({user: state.user, settings: state.settings, notifications: state.notifications})
const mapDispatchToProps = dispatch => ({
  updateUnReadToReadById: (notificationId, callback) => { dispatch(notificationActions.updateUnReadToReadById(notificationId, callback)) }
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Index))
