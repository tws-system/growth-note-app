import React, { Component } from 'react'
import { TwsReactMarkdownPreview } from 'tws-antd'
import overFlow from '../../constant/diaryOverflow'
import '../../style/App.css'
import constant from '../../constant/constant'
import {FormattedMessage} from 'react-intl'

export default class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isShowAllContent: false,
      isShowAndCloseAllContent: false
    }
  }
  componentWillReceiveProps (props) {
    this.setState({
      isShowAllContent: props.isShowAndCloseAllContent
    })
  }
  showAllContent () {
    this.setState({
      isShowAllContent: !this.state.isShowAllContent
    })
  }

  render () {
    const content = this.props.content
    return (
      <div>
        <div className='mark-down-wrap'>
          {
            this.state.isShowAllContent
              ? <TwsReactMarkdownPreview source={content} />
              : <TwsReactMarkdownPreview source={overFlow(content)} />
          }
        </div>

          <div className={content.length > constant.SHOW_MAX_DIARY_CONTENT ? 'show-all-content' : 'hidden'}
            onClick={this.showAllContent.bind(this)}>{this.state.isShowAllContent ? <FormattedMessage id='收起' /> : <FormattedMessage id='点击查看全文' />}
          </div>
      </div>
    )
  }
}
