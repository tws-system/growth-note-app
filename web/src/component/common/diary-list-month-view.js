import React, { Component } from 'react'
import { Button, Calendar, Icon, Popover } from 'antd'
import moment from 'moment'

import ReactMarkdown from 'react-markdown'
import { withRouter } from 'react-router-dom'
import { convertContent } from '../../constant/lang-util'

class DiaryListMonthView extends Component {
  constructor (props) {
    super(props)
    this.state = {
      previousMonth: moment().month(),
      previousYear: moment().year()
    }
  }

  formatListViewData = () => {
    return this.props.monthlyDiaries.map(monthlyDiary => (
      {
        id: monthlyDiary.id,
        createTime: moment(monthlyDiary.date),
        type: monthlyDiary.diaryType,
        text: monthlyDiary.content.slice(0, 13),
        content: monthlyDiary.content
      })
    )
  }

  dateCellRender = (value) => {
    const initMonthViewData = this.formatListViewData()
    const monthViewData = initMonthViewData.filter(practiseDiary =>
      (
        practiseDiary.createTime.month() === value.month() &&
        practiseDiary.createTime.date() === value.date()
      )
    )
    return (
      <ul className='events' >
        {
          monthViewData.map((item, index) => {
            let calendarButtonStyle = 'calendar__button'
            calendarButtonStyle += item.type === 'goal' ? ' calendar__button--goal' : ''
            return (
              <li key={index}>
                <Popover
                  placement='rightBottom'
                  content={<ReactMarkdown className='monthView--popover' source={item.content} />}
                  overlayStyle={{ width: 300 }}
                  trigger='click' >
                  <Button className={calendarButtonStyle}>
                    <Icon type={item.type === 'goal' ? 'environment' : 'tag'} />
                    {item.text}
                  </Button>
                </Popover>
              </li>
            )
          })
        }
      </ul>
    )
  }

  monthCellRender = (value) => {
    const monthlySubmittedNum = this.props.yearSubmitCount.find(monthlySubmitCount =>
      monthlySubmitCount.month === value.month() + 1
    )
    return monthlySubmittedNum && (
      <div className='notes--month'>
        <span>{convertContent('篇数')}</span>
        <section>{monthlySubmittedNum.count}</section>
      </div>
    )
  }

  onPanelChange = (value) => {
    const currentMonth = value.month()
    const currentYear = value.year()
    if (
      this.state.previousMonth !== currentMonth ||
      this.state.previousYear !== currentYear
    ) {
      this.props.onMonthChange(currentMonth, currentYear)

      this.setState({
        previousMonth: currentMonth,
        previousYear: currentYear
      })
    }
  }

  render () {
    return (
      <Calendar
        dateCellRender={(value) => this.dateCellRender(value)}
        monthCellRender={(value) => this.monthCellRender(value)}
        onPanelChange={(value) => this.onPanelChange(value)}
      />
    )
  }
}

export default withRouter(DiaryListMonthView)
