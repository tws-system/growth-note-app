export default {
  思特沃克学习平台:
    'https://school.thoughtworks.cn/learn/home/index.html#/app-center',
  成长日志: '/practise-diaries',
  我的日志: '/practise-diaries',
  我的关注: '/followees',
  优秀日志: '/excellent-diaries'
}
