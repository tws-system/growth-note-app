export default (state = [],action) => {
    if(action.type === 'REFRESH_YEAR_SUBMIT_COUNT') {
        return action.yearSubmitCount
    }
    return state
}