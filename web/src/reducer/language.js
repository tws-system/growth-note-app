import {autoSaveLangToLocalStorage, getLangFromLocalStorage} from '../constant/lang-util'

export default (state = '', action) => {
  switch (action.type) {
    case 'CHANGE_LANGUAGE':
      autoSaveLangToLocalStorage(action.lang)
      return getLangFromLocalStorage()
    default:
      return state
  }
}
