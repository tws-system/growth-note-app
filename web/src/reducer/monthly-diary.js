export default (state = [], action) => {
  switch (action.type) {
    case 'REFRESH_MONTHLY_DIARIES':
      return action.monthlyDiaries
    default:
      return state
  }

}
