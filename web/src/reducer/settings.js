const genSetting = data => {
  const userCenterHomeUrl = `/learn/home/index.html`
  const logoutUrl = `/learn/auth/logout`
  const notificationMoreUrl = `/learn/notification-center/index.html`
  return Object.assign(
    {},
    {
      userCenterHomeUrl,
      logoutUrl,
      notificationMoreUrl
    },
    data
  )
}

export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_SETTINGS':
      return genSetting(action.data)
    default:
      return state
  }
}
