package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.commnet;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment.Comment;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CommentRepositoryImpl implements CommentRepository {
    @Autowired
    CommentRepositoryJpaVendor repositoryJpaAdapter;

    @Override
    public List<Comment> findByPractiseDiaryId(Long id) {

        List<Comment> result = repositoryJpaAdapter.findByPractiseDiaryId(id).stream()
                .map(CommentPO::toDomain).collect(Collectors.toList());
        return result;
    }

    @Override
    public Comment save(Comment comment) {
        CommentPO commentPO = CommentPO.of(comment);
        CommentPO save = repositoryJpaAdapter.save(commentPO);
        return save.toDomain();
    }
}
