package cn.thoughtworks.school.pactise.diary.application.gateway.user;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    List<UserDto> getUsersByIds(List<Long> userIds);

    UserDto getDiaryAuthorInfo(Long userId);

    List<UserDto> getUsersLikeUsername(String username);

    List<UserDto> getUserByUserNameOrEmail(String nameOrEmail);
}
