package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.follow;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow.Follow;

import javax.persistence.*;

@Entity
@Table(name = "follow")
public class FollowPO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private Long followerId;

    @Column
    private Long followeeId;

    @Column
    private String createTime;

    public FollowPO() {
    }

    public FollowPO(Long followerId, Long followeeId) {
        this.followerId = followerId;
        this.followeeId = followeeId;
    }

    public FollowPO(Long id, Long followerId, Long followeeId, String createTime) {

        this.id = id;
        this.followerId = followerId;
        this.followeeId = followeeId;
        this.createTime = createTime;
    }

    public static FollowPO of(Follow follow) {
        return new FollowPO(follow.getId(), follow.getFollowerId(), follow.getFolloweeId(), follow.getCreateTime());
    }

    public Follow toDomain(){
        return new Follow(id, followerId, followeeId, createTime);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFollowerId() {
        return followerId;
    }

    public void setFollowerId(Long followerId) {
        this.followerId = followerId;
    }

    public Long getFolloweeId() {
        return followeeId;
    }

    public void setFolloweeId(Long followeeId) {
        this.followeeId = followeeId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
