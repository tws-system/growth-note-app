package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.follow;

import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserDto;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiary;

import java.util.List;

public class PractiseDiaryWithContactDto {
    private List<PractiseDiary> practiseDiaryList;
    private UserDto userInfo;

    public PractiseDiaryWithContactDto(List<PractiseDiary> practiseDiaryList, UserDto userInfo) {
        this.practiseDiaryList = practiseDiaryList;
        this.userInfo = userInfo;
    }

    public List<PractiseDiary> getPractiseDiaryList() {
        return practiseDiaryList;
    }

    public UserDto getUserInfo() {
        return userInfo;
    }
}
