package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.follow;

import cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.comment.CommentDto;
import cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary.PractiseDiaryRequest;

import java.util.List;

public class PractiseWithCommentsDto {
    public PractiseWithCommentsDto(PractiseDiaryRequest practiseDiary, List<CommentDto> comments) {
        this.practiseDiary = practiseDiary;
        this.comments = comments;
    }

    public PractiseDiaryRequest getPractiseDiary() {
        return practiseDiary;
    }

    public List<CommentDto> getComments() {
        return comments;
    }

    private PractiseDiaryRequest practiseDiary;

    private List<CommentDto> comments;
}
