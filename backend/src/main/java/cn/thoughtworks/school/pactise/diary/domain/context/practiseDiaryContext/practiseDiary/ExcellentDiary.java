package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary;



public class ExcellentDiary {
    private Long id;

    private String createTime;

    private Long diaryId;

    private Long operatorId;

    public ExcellentDiary(Long id, String createTime, Long diaryId, Long operatorId) {
        this.id = id;
        this.createTime = createTime;
        this.diaryId = diaryId;
        this.operatorId = operatorId;
    }

    public Long getId() {
        return id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public Long getDiaryId() {
        return diaryId;
    }

    public Long getOperatorId() {
        return operatorId;
    }

}
