package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiary;


public class ExcellentDiaryRequest {
    private Long id;

    private String createTime;

    private Long diaryId;

    private Long operatorId;


    ExcellentDiaryRequest() {
    }

    public static ExcellentDiaryRequest of(ExcellentDiary excellentDiary) {
        return new ExcellentDiaryRequest(excellentDiary.getId(),
                excellentDiary.getCreateTime(),
                excellentDiary.getDiaryId(),
                excellentDiary.getOperatorId()
        );
    }

    public ExcellentDiary toDomain() {
        return new ExcellentDiary(id, createTime, diaryId, operatorId);
    }

    public ExcellentDiaryRequest(String createTime, Long diaryId, Long operatorId) {
        this.createTime = createTime;
        this.diaryId = diaryId;
        this.operatorId = operatorId;
    }

    public ExcellentDiaryRequest(Long id, String createTime, Long diaryId, Long operatorId) {
        this.id = id;
        this.createTime = createTime;
        this.diaryId = diaryId;
        this.operatorId = operatorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(Long diaryId) {
        this.diaryId = diaryId;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }
}
