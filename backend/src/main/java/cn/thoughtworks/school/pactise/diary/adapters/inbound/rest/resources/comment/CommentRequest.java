package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.comment;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment.Comment;

public class CommentRequest {

    private Long id;

    private String commentTime;

    private String commentContent;

    private Long practiseDiaryId;

    private Long commentAuthorId;


    public CommentRequest(Long id, String commentContent, Long practiseDiaryId, Long commentAuthorId) {
        this.id = id;
        this.commentContent = commentContent;
        this.practiseDiaryId = practiseDiaryId;
        this.commentAuthorId = commentAuthorId;
    }

    public CommentRequest() {}

    public static CommentRequest of(Comment comment) {
        return new CommentRequest(comment.getId(), comment.getCommentContent(), comment.getPractiseDiaryId(), comment.getCommentAuthorId());
    }

    public Long getCommentAuthorId() {
        return commentAuthorId;
    }

    public Long getPractiseDiaryId() {

        return practiseDiaryId;
    }

    public String getCommentContent() {

        return commentContent;
    }

    public String getCommentTime() {

        return commentTime;
    }

    public Long getId() {

        return id;
    }


    public void setCommentAuthorId(Long commentAuthorId) {
        this.commentAuthorId = commentAuthorId;
    }

    public void setPractiseDiaryId(Long practiseDiaryId) {

        this.practiseDiaryId = practiseDiaryId;
    }

    public void setCommentContent(String commentContent) {

        this.commentContent = commentContent;
    }

    public void setCommentTime(String commentTime) {

        this.commentTime = commentTime;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String toString() {
        return this.commentContent;
    }

    public Comment toDomain(){
        return new Comment(id, commentTime, commentContent, practiseDiaryId, commentAuthorId);
    }
}
