package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;

public class PractiseDiaryCreateDto {
    private String uri;

    public PractiseDiaryCreateDto(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }
}
