package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment;


import java.util.List;

public interface CommentRepository {
    List<Comment> findByPractiseDiaryId(Long id);
    
    Comment save(Comment comment);
}
