package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.follow;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow.Follow;

public class FollowRequest {

    private Long id;

    private Long followerId;

    private Long followeeId;

    private String createTime;

    public FollowRequest() {
    }

    public FollowRequest(Long followerId, Long followeeId) {
        this.followerId = followerId;
        this.followeeId = followeeId;
    }

    public FollowRequest(Long id, Long followerId, Long followeeId) {
        this.id = id;
        this.followerId = followerId;
        this.followeeId = followeeId;
    }

    public static FollowRequest of(Follow follow) {
        return new FollowRequest(follow.getId(), follow.getFollowerId(), follow.getFolloweeId());
    }

    public Follow toDomain(){
        return new Follow(id, followerId, followeeId, createTime);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFollowerId() {
        return followerId;
    }

    public void setFollowerId(Long followerId) {
        this.followerId = followerId;
    }

    public Long getFolloweeId() {
        return followeeId;
    }

    public void setFolloweeId(Long followeeId) {
        this.followeeId = followeeId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
