package cn.thoughtworks.school.pactise.diary.application.usecases.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiary;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiaryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QueryExcellentDiaryUseCase {

    public final ExcellentDiaryRepository excellentDiaryRepository;

    public QueryExcellentDiaryUseCase(ExcellentDiaryRepository excellentDiaryRepository) {
        this.excellentDiaryRepository = excellentDiaryRepository;
    }

    public ExcellentDiary findByDiaryId(long id) {
        return excellentDiaryRepository.findByDiaryId(id);
    }

    public Page<ExcellentDiary> findAll(Pageable pageable) {
        return excellentDiaryRepository.findAll(pageable);
    }

    public List<ExcellentDiary> findAll() {
        return excellentDiaryRepository.findAll();
    }
}
