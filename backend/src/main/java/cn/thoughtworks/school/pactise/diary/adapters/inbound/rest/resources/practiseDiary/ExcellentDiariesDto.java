package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;

import java.util.List;

public class ExcellentDiariesDto {
    private Integer total;
    private List<ExcellentPractiseDiaryDto> excellentDiariesAndComments;

    public ExcellentDiariesDto(Integer total, List<ExcellentPractiseDiaryDto> excellentDiariesAndComments) {
        this.total = total;
        this.excellentDiariesAndComments = excellentDiariesAndComments;
    }

    public Integer getTotal() {
        return total;
    }

    public List<ExcellentPractiseDiaryDto> getExcellentDiariesAndComments() {
        return excellentDiariesAndComments;
    }
}
