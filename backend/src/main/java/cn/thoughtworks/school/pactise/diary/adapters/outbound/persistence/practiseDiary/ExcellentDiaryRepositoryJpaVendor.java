package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.practiseDiary;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExcellentDiaryRepositoryJpaVendor extends JpaRepository<ExcellentDiaryPO, Long> {
    Page<ExcellentDiaryPO> findAll(Pageable pageable);
    ExcellentDiaryPO findByDiaryId(Long excellentDiaryId);
}
