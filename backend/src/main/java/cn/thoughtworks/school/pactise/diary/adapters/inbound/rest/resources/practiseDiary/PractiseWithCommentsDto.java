package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;

import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserDto;

import java.util.List;

public class PractiseWithCommentsDto {
    private Integer total;
    private List<cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.follow.PractiseWithCommentsDto> practiseDiaryAndComments;
    private UserDto userInfo;

    public Integer getTotal() {
        return total;
    }

    public List<cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.follow.PractiseWithCommentsDto> getPractiseDiaryAndComments() {
        return practiseDiaryAndComments;
    }

    public UserDto getUserInfo() {
        return userInfo;
    }

    public PractiseWithCommentsDto(Integer total, List<cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.follow.PractiseWithCommentsDto> practiseDiaryAndComments, UserDto userInfo) {
        this.total = total;
        this.practiseDiaryAndComments = practiseDiaryAndComments;
        this.userInfo = userInfo;
    }
}
