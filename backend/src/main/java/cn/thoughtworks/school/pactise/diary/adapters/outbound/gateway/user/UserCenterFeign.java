package cn.thoughtworks.school.pactise.diary.adapters.outbound.gateway.user;

import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@FeignClient(name="${feign.userCenter.name}",
        url = "${feign.userCenter.url}")
@Service
public interface UserCenterFeign {

    @GetMapping("/api/users/ids/{ids}")
    List<UserDto> getUsersByIds(@PathVariable("ids") String ids);

    @GetMapping("/api/users/{userId}")
    UserDto getUserById(@PathVariable("userId") Long userId);

    @GetMapping("/api/users/query")
    List<UserDto> getUsersLikeUsername(@RequestParam("username") String username);

    @GetMapping("/api/users")
    List<UserDto> getUserByUserNameOrEmail(@RequestParam("nameOrEmail") String nameOrEmail);
}
