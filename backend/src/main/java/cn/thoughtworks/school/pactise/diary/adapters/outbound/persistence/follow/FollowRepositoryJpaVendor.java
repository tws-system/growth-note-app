package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.follow;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FollowRepositoryJpaVendor extends JpaRepository<FollowPO, Long> {

    List<FollowPO> findByFollowerId(Long followerId);

    FollowPO findByFollowerIdAndFolloweeId(Long followerId, Long followeeId);

    List<FollowPO> findByFolloweeId(Long followeeId);
}
