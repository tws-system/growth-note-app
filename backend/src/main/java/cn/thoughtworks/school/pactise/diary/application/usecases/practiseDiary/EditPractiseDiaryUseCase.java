package cn.thoughtworks.school.pactise.diary.application.usecases.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiary;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiaryService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EditPractiseDiaryUseCase {

    public final PractiseDiaryService practiseDiaryService;

    public EditPractiseDiaryUseCase(PractiseDiaryService practiseDiaryService) {
        this.practiseDiaryService = practiseDiaryService;
    }

    public PractiseDiary save(PractiseDiary practiseDiary) {
        return practiseDiaryService.save(practiseDiary);
    }

    public void deleteById(long id) {
        practiseDiaryService.delete(id);
    }
}
