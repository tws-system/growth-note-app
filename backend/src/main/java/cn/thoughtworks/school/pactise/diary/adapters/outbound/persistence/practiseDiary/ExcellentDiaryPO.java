package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiary;

import javax.persistence.*;

@Entity
@Table(name = "excellentDiary")
public class ExcellentDiaryPO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String createTime;

    @Column
    private Long diaryId;

    @Column
    private Long operatorId;


    ExcellentDiaryPO() {
    }

    public static ExcellentDiaryPO of(ExcellentDiary excellentDiary) {
        return new ExcellentDiaryPO(excellentDiary.getId(),
                excellentDiary.getCreateTime(),
                excellentDiary.getDiaryId(),
                excellentDiary.getOperatorId()
        );
    }

    public ExcellentDiary toDomain() {
        return new ExcellentDiary(id, createTime, diaryId, operatorId);
    }

    public ExcellentDiaryPO(String createTime, Long diaryId, Long operatorId) {
        this.createTime = createTime;
        this.diaryId = diaryId;
        this.operatorId = operatorId;
    }

    public ExcellentDiaryPO(Long id, String createTime, Long diaryId, Long operatorId) {
        this.id = id;
        this.createTime = createTime;
        this.diaryId = diaryId;
        this.operatorId = operatorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getDiaryId() {
        return diaryId;
    }

    public void setDiaryId(Long diaryId) {
        this.diaryId = diaryId;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }
}
