package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.follow;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow.Follow;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow.FollowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class FollowRepositoryImpl implements FollowRepository {
    @Autowired
    FollowRepositoryJpaVendor repositoryJpaVendor;

    @Override
    public List<Follow> findByFollowerId(Long followerId) {
        return repositoryJpaVendor.findByFollowerId(followerId).stream().map(FollowPO::toDomain).collect(Collectors.toList());
    }

    @Override
    public Follow findByFollowerIdAndFolloweeId(Long followerId, Long followeeId) {
        FollowPO byFollowerIdAndFolloweeId = repositoryJpaVendor.findByFollowerIdAndFolloweeId(followerId, followeeId);
        if (byFollowerIdAndFolloweeId == null) {
            return null;
        }
        return byFollowerIdAndFolloweeId.toDomain();
    }

    @Override
    public List<Follow> findByFolloweeId(Long followeeId) {
        return repositoryJpaVendor.findByFolloweeId(followeeId).stream().map(FollowPO::toDomain).collect(Collectors.toList());
    }

    @Override
    public Follow save(Follow contactUser) {
        return repositoryJpaVendor.save(FollowPO.of(contactUser)).toDomain();
    }

    @Override
    public void deleteById(Long id) {
        repositoryJpaVendor.deleteById(id);
    }
}
