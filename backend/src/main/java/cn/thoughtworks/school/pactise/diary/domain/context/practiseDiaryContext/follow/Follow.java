package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow;

public class Follow {
    private Long id;

    private Long followerId;

    private Long followeeId;

    private String createTime;

    public Follow(Long followerId, Long followeeId, String createTime) {
        this.id = id;
        this.followerId = followerId;
        this.followeeId = followeeId;
        this.createTime = createTime;
    }

    public Follow(Long id, Long followerId, Long followeeId) {
        this.id = id;
        this.followerId = followerId;
        this.followeeId = followeeId;
    }

    public Follow(Long id, Long followerId, Long followeeId, String createTime) {

        this.id = id;
        this.followerId = followerId;
        this.followeeId = followeeId;
        this.createTime = createTime;
    }

    public Long getId() {
        return id;
    }

    public Long getFollowerId() {
        return followerId;
    }

    public Long getFolloweeId() {
        return followeeId;
    }

    public String getCreateTime() {
        return createTime;
    }
}
