package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary;

import org.springframework.data.domain.Pageable;

import javax.persistence.Tuple;
import java.util.Date;
import java.util.List;

public interface PractiseDiaryRepository {

    List<PractiseDiary> findByAuthorId(Long authorId, Pageable pageable);

    List<PractiseDiary> findByAuthorIdOrderByDateDesc(Long authorId);

    List<PractiseDiary> findAllByIdIn(List<Long> ids);

    List<PractiseDiary> findByAuthorIdAndDateBetween(Long authorId, Date startDate, Date endDate);

    List<Tuple> getMonthlyCountByAuthorIdAndYearBetween(Long authorId, Date startYear, Date endYear);

    PractiseDiary save(PractiseDiary practiseDiary);

    boolean existsById(long id);

    void deleteById(long id);

    PractiseDiary findById(long id);
}
