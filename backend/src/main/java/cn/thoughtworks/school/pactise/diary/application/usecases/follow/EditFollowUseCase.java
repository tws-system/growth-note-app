package cn.thoughtworks.school.pactise.diary.application.usecases.follow;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow.Follow;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow.FollowRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EditFollowUseCase {
    private final FollowRepository followRepository;

    public EditFollowUseCase(FollowRepository followRepository) {
        this.followRepository = followRepository;
    }

    public Follow save(Follow follow) {
        return followRepository.save(follow);
    }

    public void deleteById(Long id) {
        followRepository.deleteById(id);
    }
}
