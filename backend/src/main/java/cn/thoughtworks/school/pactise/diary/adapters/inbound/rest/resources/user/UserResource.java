package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.user;

import cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.auth.annotation.Auth;
import cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.auth.configrations.AuthResolver;
import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserDto;
import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class UserResource {

    private static Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity<UserDto> getUserById(@Auth AuthResolver.User user) {
        log.info(String.format("current user is: %s", user));
        UserDto result = userService.getDiaryAuthorInfo(user.getId());
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<List<UserDto>> getUsersLikeUsername(@RequestParam(value = "username") String username) {
        List<UserDto> result = userService.getUsersLikeUsername(username);
        return ResponseEntity.ok(result);
    }

}
