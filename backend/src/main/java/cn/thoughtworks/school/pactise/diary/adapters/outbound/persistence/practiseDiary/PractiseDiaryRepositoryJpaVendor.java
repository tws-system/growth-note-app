package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.practiseDiary;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import java.util.Date;
import java.util.List;

@Repository
public interface PractiseDiaryRepositoryJpaVendor extends JpaRepository<PractiseDiaryPO, Long> {

    List<PractiseDiaryPO> findByAuthorId(Long authorId, Pageable pageable);

    List<PractiseDiaryPO> findByAuthorIdOrderByDateDesc(Long authorId);

    List<PractiseDiaryPO> findAllByIdIn(List<Long> ids);

    List<PractiseDiaryPO> findByAuthorIdAndDateBetween(Long authorId, Date startDate, Date endDate);

    @Query(value = "SELECT COUNT(*) as count,MONTH(p.date) as month FROM practiseDiary p where p.authorId = ?1 and p.date between ?2 and ?3 GROUP BY MONTH(p.date)", nativeQuery = true)
    List<Tuple> getMonthlyCountByAuthorIdAndYearBetween(Long authorId, Date startYear, Date endYear);

}
