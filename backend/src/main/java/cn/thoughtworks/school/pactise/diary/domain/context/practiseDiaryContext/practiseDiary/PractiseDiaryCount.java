package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary;

public class PractiseDiaryCount {

    private int month;
    private long count;

    public PractiseDiaryCount(int month, long count) {
        this.month = month;
        this.count = count;
    }

    public PractiseDiaryCount() {
    }

    public int getMonth() {
        return month;
    }


    public long getCount() {
        return count;
    }


}
