package cn.thoughtworks.school.pactise.diary.adapters.outbound.gateway.user;

import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserDto;
import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService {
    private final UserCenterFeign userCenterFeign;

    public UserServiceImpl(UserCenterFeign userCenterFeign) {
        this.userCenterFeign = userCenterFeign;
    }

    public List<UserDto> getUsersByIds(List<Long> userIds) {
        if (userIds.isEmpty()) {
            return new ArrayList<>();
        }
        String ids = String.join(",", userIds.stream().map(Object::toString).collect(Collectors.toList()));
        return userCenterFeign.getUsersByIds(ids);
    }

    public UserDto getDiaryAuthorInfo(Long userId) {
        return userCenterFeign.getUserById(userId);
    }

    public List<UserDto> getUsersLikeUsername(String username) {
        return userCenterFeign.getUsersLikeUsername(username);
    }

    public List<UserDto> getUserByUserNameOrEmail(String nameOrEmail) {
        return userCenterFeign.getUserByUserNameOrEmail(nameOrEmail);
    }
}
