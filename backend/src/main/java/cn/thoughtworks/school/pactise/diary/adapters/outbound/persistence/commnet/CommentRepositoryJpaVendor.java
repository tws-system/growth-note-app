package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.commnet;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepositoryJpaVendor extends JpaRepository<CommentPO, Long> {
    List<CommentPO> findByPractiseDiaryId(Long id);
}
