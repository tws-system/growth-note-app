package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;


import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiary;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class PractiseDiaryDto {
    private Long id;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date date;
    private String diaryType;
    private String content;

    public PractiseDiaryDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDiaryType() {
        return diaryType;
    }

    public void setDiaryType(String diaryType) {
        this.diaryType = diaryType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static PractiseDiaryDto toResource(PractiseDiary practiseDiary) {
        PractiseDiaryDto practiseDiaryResource = new PractiseDiaryDto();

        practiseDiaryResource.setId(practiseDiary.getId());
        practiseDiaryResource.setContent(practiseDiary.getContent());
        practiseDiaryResource.setDate(practiseDiary.getDate());
        practiseDiaryResource.setDiaryType(practiseDiary.getDiaryType());

        return practiseDiaryResource;
    }
}
