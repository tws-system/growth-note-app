package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiary;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class PractiseDiaryRequest {

    private Long id;

    private String createTime;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date date;

    private String content;

    private Long authorId;

    private String diaryType;

    public PractiseDiaryRequest(Long id, String createTime, Date date, String content, Long authorId, String diaryType) {
        this.id = id;
        this.createTime = createTime;
        this.date = date;
        this.content = content;
        this.authorId = authorId;
        this.diaryType = diaryType;
    }

    public static PractiseDiaryRequest of(PractiseDiary practiseDiary) {
        return new PractiseDiaryRequest(practiseDiary.getId(), practiseDiary.getCreateTime(), practiseDiary.getDate(), practiseDiary.getContent(),
                practiseDiary.getAuthorId(), practiseDiary.getDiaryType());
    }

    public PractiseDiary toDomain(){
        return new PractiseDiary(id,  createTime,  date,  content,  authorId, diaryType);
    }

    PractiseDiaryRequest() {

    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getId() {
        return id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public Date getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public String toString() {
        return this.content;
    }

    public String getDiaryType() {
        return diaryType;
    }

    public void setDiaryType(String diaryType) {
        this.diaryType = diaryType;
    }
}
