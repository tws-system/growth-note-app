package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.follow;

import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserDto;

import java.util.List;

public class FolloweePractiseDiaryWithCommentsDto {
    private Integer total;
    private List<PractiseWithCommentsDto> practiseDiaryAndComments;
    private UserDto followeeInfo;

    public FolloweePractiseDiaryWithCommentsDto(Integer total, List<PractiseWithCommentsDto> practiseDiaryAndComments, UserDto followeeInfo) {
        this.total = total;
        this.practiseDiaryAndComments = practiseDiaryAndComments;
        this.followeeInfo = followeeInfo;
    }

    public Integer getTotal() {
        return total;
    }

    public List<PractiseWithCommentsDto> getPractiseDiaryAndComments() {
        return practiseDiaryAndComments;
    }

    public UserDto getFolloweeInfo() {
        return followeeInfo;
    }
}
