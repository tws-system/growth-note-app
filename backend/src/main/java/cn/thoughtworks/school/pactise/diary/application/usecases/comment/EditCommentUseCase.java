package cn.thoughtworks.school.pactise.diary.application.usecases.comment;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment.Comment;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment.CommentRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EditCommentUseCase {
    private final CommentRepository commentRepository;

    public EditCommentUseCase(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }
}
