package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiary;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ExcellentDiaryRepositoryImpl implements ExcellentDiaryRepository {

    @Autowired
    ExcellentDiaryRepositoryJpaVendor repositoryJpaVendor;

    @Override
    public Page<ExcellentDiary> findAll(Pageable pageable) {
        return repositoryJpaVendor.findAll(pageable).map(ExcellentDiaryPO::toDomain);
    }

    @Override
    public ExcellentDiary findByDiaryId(Long excellentDiaryId) {
        ExcellentDiaryPO byDiaryIdPO = repositoryJpaVendor.findByDiaryId(excellentDiaryId);
        if (byDiaryIdPO == null) {
            return null;
        }
        return byDiaryIdPO.toDomain();
    }

    @Override
    public void deleteById(Long id) {
        repositoryJpaVendor.deleteById(id);
    }

    @Override
    public ExcellentDiary save(ExcellentDiary excellentDiary) {
        return repositoryJpaVendor.save(ExcellentDiaryPO.of(excellentDiary)).toDomain();
    }

    @Override
    public List<ExcellentDiary> findAll() {
        return repositoryJpaVendor.findAll().stream().map(ExcellentDiaryPO::toDomain).collect(Collectors.toList());
    }
}
