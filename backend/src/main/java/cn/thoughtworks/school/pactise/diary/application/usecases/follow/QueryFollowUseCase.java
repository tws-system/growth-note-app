package cn.thoughtworks.school.pactise.diary.application.usecases.follow;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow.Follow;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow.FollowRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QueryFollowUseCase {
    private final FollowRepository followRepository;

    public QueryFollowUseCase(FollowRepository followRepository) {
        this.followRepository = followRepository;
    }


    public Follow findByFollowerIdAndFolloweeId(Long id, Long followeeId) {
        return followRepository.findByFollowerIdAndFolloweeId(id, followeeId);
    }

    public List<Follow> findByFollowerId(Long id) {
        return followRepository.findByFollowerId(id);
    }

    public List<Follow> findByFolloweeId(Long id) {
        return followRepository.findByFolloweeId(id);
    }
}
