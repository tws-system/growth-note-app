package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary;

import java.util.Date;

public class PractiseDiary {
    private Long id;

    private String createTime;

    private Date date;

    private String content;

    private Long authorId;

    private String diaryType;

    public PractiseDiary(Long id, String createTime, Date date, String content, Long authorId, String diaryType) {
        this.id = id;
        this.createTime = createTime;
        this.date = date;
        this.content = content;
        this.authorId = authorId;
        this.diaryType = diaryType;
    }

    public Long getId() {
        return id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public Date getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public String toString() {
        return this.content;
    }

    public String getDiaryType() {
        return diaryType;
    }

    public void setContent(String content) {

        this.content = content;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
