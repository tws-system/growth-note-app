package cn.thoughtworks.school.pactise.diary.application.usecases.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiary;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiaryRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EditExcellentDiaryUseCase {

    public final ExcellentDiaryRepository excellentDiaryRepository;

    public EditExcellentDiaryUseCase(ExcellentDiaryRepository excellentDiaryRepository) {
        this.excellentDiaryRepository = excellentDiaryRepository;
    }

    public void deleteById(Long id) {
        excellentDiaryRepository.deleteById(id);
    }

    public ExcellentDiary save(ExcellentDiary excellentDiary) {
        return excellentDiaryRepository.save(excellentDiary);
    }
}
