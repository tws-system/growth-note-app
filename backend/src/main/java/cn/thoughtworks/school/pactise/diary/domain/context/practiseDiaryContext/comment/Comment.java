package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment;


public class Comment {

    private Long id;

    private String commentTime;

    private String commentContent;

    private Long practiseDiaryId;

    private Long commentAuthorId;


    public Comment(Long id, String commentTime, String commentContent, Long practiseDiaryId, Long commentAuthorId) {
        this.id = id;
        this.commentTime = commentTime;
        this.commentContent = commentContent;
        this.practiseDiaryId = practiseDiaryId;
        this.commentAuthorId = commentAuthorId;
    }

    public Comment() {}

    public Long getCommentAuthorId() {
        return commentAuthorId;
    }

    public Long getPractiseDiaryId() {

        return practiseDiaryId;
    }

    public String getCommentContent() {

        return commentContent;
    }

    public String getCommentTime() {

        return commentTime;
    }

    public Long getId() {

        return id;
    }


    public void setId(Long id) {

        this.id = id;
    }

    public String toString() {
        return this.commentContent;
    }

}
