package cn.thoughtworks.school.pactise.diary.application.usecases.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiary;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiaryCount;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiaryRepository;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiaryService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QueryPractiseDiaryUseCase {
    private final PractiseDiaryRepository practiseDiaryRepository;
    private final PractiseDiaryService practiseDiaryService;

    public QueryPractiseDiaryUseCase(PractiseDiaryRepository practiseDiaryRepository, PractiseDiaryService practiseDiaryService) {
        this.practiseDiaryRepository = practiseDiaryRepository;
        this.practiseDiaryService = practiseDiaryService;
    }

    public List<PractiseDiary> findByAuthorId(Long id, Pageable pageable) {
        return practiseDiaryRepository.findByAuthorId(id, pageable);
    }

    public List<PractiseDiary> findByAuthorIdOrderByDateDesc(Long id) {
        return practiseDiaryRepository.findByAuthorIdOrderByDateDesc(id);
    }

    public boolean existsById(long id) {
        return practiseDiaryRepository.existsById(id);
    }

    public PractiseDiary findById(long id) {
        return practiseDiaryRepository.findById(id);
    }

    public List<PractiseDiary> findMonthlyDiariesByAuthorId(Long id, int year, int month) {
        return practiseDiaryService.findMonthlyDiariesByAuthorId(id, year, month);
    }

    public List<PractiseDiaryCount> findYearSubmitCountByAuthorId(Long id, int year) {
        return practiseDiaryService.findYearSubmitCountByAuthorId(id, year);
    }

    public List<PractiseDiary> findAllByIdIn(List<Long> diaryIds) {
        return practiseDiaryRepository.findAllByIdIn(diaryIds);
    }
}
