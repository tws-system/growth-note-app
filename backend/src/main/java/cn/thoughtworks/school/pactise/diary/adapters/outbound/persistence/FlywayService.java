package cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("test")
public class FlywayService {

    @Value("${database.url}")
    private String DbUrl;

    @Value("${database.username}")
    private String DbUser;

    @Value("${database.password}")
    private String DbPassword;

    public void migrateDatabase() throws Exception {
        Flyway flyway = new Flyway();
        flyway.setDataSource(DbUrl, DbUser, DbPassword);
        flyway.setLocations("classpath:db/migration");
        flyway.clean();
        flyway.migrate();
    }
}