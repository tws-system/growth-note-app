package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.comment;

public class CreateCommentDto {
    private String uri;

    public CreateCommentDto(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
