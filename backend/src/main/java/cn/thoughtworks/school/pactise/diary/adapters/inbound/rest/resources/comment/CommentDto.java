package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.comment;

import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserDto;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment.Comment;

public class CommentDto {
    private UserDto commentAuthorInfo;
    private Comment comment;

    public UserDto getCommentAuthorInfo() {
        return commentAuthorInfo;
    }

    public Comment getComment() {
        return comment;
    }

    public CommentDto(UserDto commentAuthorInfo, Comment comment) {
        this.commentAuthorInfo = commentAuthorInfo;
        this.comment = comment;
    }
}
