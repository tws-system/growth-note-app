package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.follow;


import java.util.List;

public interface FollowRepository {

    List<Follow> findByFollowerId(Long followerId);

    Follow findByFollowerIdAndFolloweeId(Long followerId, Long followeeId);

    List<Follow> findByFolloweeId(Long followeeId);

    Follow save(Follow contactUser);

    void deleteById(Long id);
}
