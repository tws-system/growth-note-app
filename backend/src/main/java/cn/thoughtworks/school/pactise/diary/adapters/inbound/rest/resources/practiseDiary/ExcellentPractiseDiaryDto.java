package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;

import cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.comment.CommentDto;
import cn.thoughtworks.school.pactise.diary.application.gateway.user.UserDto;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiary;

import java.util.List;

public class ExcellentPractiseDiaryDto {
    private PractiseDiary excellentDiary;
    private List<CommentDto> comments;
    private UserDto diaryAuthorInfo;


    public List<CommentDto> getComments() {
        return comments;
    }

    public UserDto getDiaryAuthorInfo() {
        return diaryAuthorInfo;
    }

    public ExcellentPractiseDiaryDto(PractiseDiary excellentDiary, List<CommentDto> comments, UserDto diaryAuthorInfo) {
        this.excellentDiary = excellentDiary;
        this.comments = comments;
        this.diaryAuthorInfo = diaryAuthorInfo;
    }

    public PractiseDiary getExcellentDiary() {
        return excellentDiary;
    }
}
