package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;

import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiary;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PractiseDiaryResourceAssembler {

    public List<PractiseDiaryDto> toResources(List<PractiseDiary> practiseDiaries) {
        return practiseDiaries.stream()
                .map(PractiseDiaryDto::toResource)
                .collect(Collectors.toList());
    }
}
