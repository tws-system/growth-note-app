package cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ExcellentDiaryRepository {
    Page<ExcellentDiary> findAll(Pageable pageable);
    ExcellentDiary findByDiaryId(Long excellentDiaryId);

    void deleteById(Long id);

    ExcellentDiary save(ExcellentDiary excellentDiary);

    List<ExcellentDiary> findAll();
}
