package cn.thoughtworks.school.pactise.diary.adapters.inbound.rest.resources.practiseDiary;

public class ExcellentDiaryCreateDto {
    private String uri;

    public ExcellentDiaryCreateDto(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }
}
