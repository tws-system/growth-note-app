
import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        urlPath(value(consumer(regex('/api/followees/tutors')), producer('/api/followees/tutors')))
        headers {
            header('id', value(consumer(regex('\\d+')), producer(2)))
        }
    }
    response {
        status 200
        body("""
            [{
                "id": 1,
                "username": "zhang",
                "email": "zhang@qq.com",
                "mobilePhone": "123345678901",
                "roles": [2]
            }]
        """)
        bodyMatchers {
            jsonPath('$[*]', byType())
            jsonPath('$[*].id', byRegex(number()))
            jsonPath('$[*].username', byRegex(nonEmpty()))
            jsonPath('$[*].email', byRegex(nonEmpty()))
            jsonPath('$[*].mobilePhone', byRegex(nonEmpty()))
            jsonPath('$[*].roles', byType())
        }
        headers {
            header('Content-Type': value(
                    producer(regex('application/json.*')),
                    consumer('application/json')
            ))
        }
    }
}


