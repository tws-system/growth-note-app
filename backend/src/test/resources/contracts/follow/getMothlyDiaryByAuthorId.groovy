
import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        urlPath(value(consumer(regex('/api/followees/[0-9]+/practise-diaries')), producer('/api/followees/2/practise-diaries'))) {
            queryParameters {
                parameter 'year': value(consumer(matching("[0-9]+")), producer(2018))
                parameter 'month': value(consumer(matching("[1-9]+")), producer(1))
            }
        }
        headers {
            header('id', value(consumer(regex('\\d+')), producer(1)))
        }
    }
    response {
        status 200
        body("""
            [{
                "id": 1,
                "date": "2018-01-01",
                "content": "hfksdj",
                "diaryType": "diaryType"
            }]
        """)
        bodyMatchers {
            jsonPath('$[*]', byType())
            jsonPath('$[*].id', byRegex(number()))
            jsonPath('$[*].date', byRegex(nonEmpty()))
            jsonPath('$[*].content', byRegex(nonEmpty()))
            jsonPath('$[*].diaryType', byRegex(nonEmpty()))
        }
        headers {
            header('Content-Type': value(
                    producer(regex('application/json.*')),
                    consumer('application/json')
            ))
        }
    }
}