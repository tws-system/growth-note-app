
import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        urlPath(value(consumer(regex('/api/followees/[0-9]+/practise-diaries')), producer('/api/followees/2/practise-diaries'))) {
            queryParameters {
                parameter 'page': value(consumer(matching("[0-9]+")), producer(1))
                parameter 'pageSize': value(consumer(matching("[1-9]+")), producer(20))
            }
        }
        headers {
            header('id', value(consumer(regex('\\d+')), producer(1)))
        }
    }
    response {
        status 200
        body("""
{
  "followeeInfo": {
    "mobilePhone": "12345678901",
    "roles": [
      2
    ],
    "id": 1,
    "userName": "zhang",
    "email": "zhang@qq.com"
  },
  "total": 1,
  "practiseDiaryAndComments": [
    {
      "comments": [],
      "practiseDiary": {
        "id": 1,
        "createTime": "2012-12-12 00:00:00.0",
        "date": "2018-02-08",
        "content": "hfksdj",
        "authorId": 2,
        "diaryType": "diaryType"
      }
    }
  ]
}
        """)
        bodyMatchers {
            jsonPath('$.followeeInfo.id', byRegex(number()))
            jsonPath('$.total', byRegex(number()))
            jsonPath('$.practiseDiaryAndComments.[*].practiseDiary',byType())
            jsonPath('$.practiseDiaryAndComments.[*].practiseDiary.id', byRegex(number()))
            jsonPath('$.practiseDiaryAndComments.[*].practiseDiary.authorId', byRegex(number()))
            jsonPath('$.practiseDiaryAndComments.[*].practiseDiary.diaryType', byRegex(nonEmpty()))
            jsonPath('$.practiseDiaryAndComments.[*].practiseDiary.createTime', byRegex(nonEmpty()))
            jsonPath('$.practiseDiaryAndComments.[*].practiseDiary.date', byRegex(nonEmpty()))
            jsonPath('$.practiseDiaryAndComments.[*].practiseDiary.content', byRegex(nonEmpty()))
        }
        headers {
            header('Content-Type': value(
                    producer(regex('application/json.*')),
                    consumer('application/json')
            ))
        }
    }
}