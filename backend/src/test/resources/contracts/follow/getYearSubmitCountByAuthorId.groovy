
import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        urlPath(value(consumer(regex('/api/followees/[0-9]+/practise-diaries/monthly-count')), producer('/api/followees/2/practise-diaries/monthly-count'))) {
            queryParameters {
                parameter 'year': value(consumer(matching("[0-9]+")), producer(2018))
            }
        }
        headers {
            header('id', value(consumer(regex('\\d+')), producer(1)))
        }
    }
    response {
        status 200
        body("""
            [{
                "month": 2,
                "count": 1
            }]
        """)
        bodyMatchers {
            jsonPath('$[*]', byType())
            jsonPath('$[*].month', byRegex(number()))
            jsonPath('$[*].count', byRegex(number()))
        }
        headers {
            header('Content-Type': value(
                    producer(regex('application/json.*')),
                    consumer('application/json')
            ))
        }
    }
}