import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method "GET"
        url '/api/excellentDiaries'
    }
    response {
        status 200
        body("""
            {
              "excellentDiariesAndComments": [
                {
                  "comments": [{
                  }],
                  "excellentDiary": {
                  },
                  "diaryAuthorInfo": {
                  }
                }
              ]
            }
        """)
        bodyMatchers {
            jsonPath('$.excellentDiariesAndComments', byType())
            jsonPath('$.excellentDiariesAndComments[*]', byType())
            jsonPath('$.excellentDiariesAndComments[*].excellentDiary', byType())
            jsonPath('$.excellentDiariesAndComments[*].diaryAuthorInfo', byType())
            jsonPath('$.excellentDiariesAndComments[*].comments', byType())
        }
        headers {
            contentType(applicationJsonUtf8())
        }
    }
}