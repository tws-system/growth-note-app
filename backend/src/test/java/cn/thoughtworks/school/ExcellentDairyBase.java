package cn.thoughtworks.school;

import cn.thoughtworks.school.pactise.diary.BackendApplication;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.comment.CommentRepository;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.ExcellentDiaryRepository;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiaryRepository;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.commnet.CommentPO;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.practiseDiary.ExcellentDiaryPO;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.practiseDiary.PractiseDiaryPO;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.FlywayService;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.util.Calendar;

@Ignore
@SpringBootTest(classes = BackendApplication.class)
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner
@ActiveProfiles("test")
public class ExcellentDairyBase {

    @Autowired
    private ExcellentDiaryRepository excellentDiaryRepository;

    @Autowired
    private PractiseDiaryRepository practiseDiaryRepository;

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    WebApplicationContext wac;
    @Autowired
    private FlywayService flywayService;

    private static boolean cleanFlag = true;

    @Before
    public void setup() throws Exception {
        if (cleanFlag) {
            flywayService.migrateDatabase();
            buildData();
            cleanFlag = false;
        }

        RestAssuredMockMvc.webAppContextSetup(wac);
    }

    private void buildData() {

        String createTime = "2018-01-20";
        Long diaryId = (long) 1;
        Long operatorId = (long) 1;
        excellentDiaryRepository.save(new ExcellentDiaryPO(createTime, diaryId, operatorId).toDomain());

        Long id = (long) 1;
        String createTime1 = "12:12:12";
        final Calendar dateInstance = Calendar.getInstance();
        dateInstance.set(2018, Calendar.JANUARY, 1);
        String content = "content";
        Long authorId = (long) 1;
        practiseDiaryRepository.save(new PractiseDiaryPO(id, createTime1, dateInstance.getTime(), content, authorId, "diaryType").toDomain());


        CommentPO comment = new CommentPO();
        comment.setCommentAuthorId(id);
        comment.setCommentContent("commentContent");
        comment.setCommentTime(createTime);
        comment.setPractiseDiaryId(id);
        comment.setId(id);
        commentRepository.save(comment.toDomain());

    }
}
