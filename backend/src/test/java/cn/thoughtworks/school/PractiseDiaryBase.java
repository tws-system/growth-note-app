package cn.thoughtworks.school;

import cn.thoughtworks.school.pactise.diary.BackendApplication;
import cn.thoughtworks.school.pactise.diary.domain.context.practiseDiaryContext.practiseDiary.PractiseDiaryRepository;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.practiseDiary.PractiseDiaryPO;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.FlywayService;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.util.Calendar;

@Ignore
@SpringBootTest(classes = BackendApplication.class)
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner
@ActiveProfiles("test")
public class PractiseDiaryBase {
    @Autowired
    WebApplicationContext wac;

    @Autowired
    private FlywayService flywayService;

    @Autowired
    private PractiseDiaryRepository practiseDiaryRepository;

    private static boolean cleanFlag = true;

    @Before
    public void setup() throws Exception {
        if (cleanFlag) {
            flywayService.migrateDatabase();
            cleanFlag = false;

            String createTime = "12:12:12";
            Calendar dateInstance = Calendar.getInstance();
            dateInstance.set(2018, Calendar.JANUARY, 1);
            String content = "content";
            Long authorId = (long) 1;
            practiseDiaryRepository.save(new PractiseDiaryPO(createTime, dateInstance.getTime(), content, authorId, "diary").toDomain());
            practiseDiaryRepository.save(new PractiseDiaryPO(createTime, dateInstance.getTime(), content, authorId, "diary").toDomain());
        }

        RestAssuredMockMvc.webAppContextSetup(wac);
    }
}
