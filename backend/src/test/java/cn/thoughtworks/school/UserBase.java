package cn.thoughtworks.school;

import cn.thoughtworks.school.pactise.diary.BackendApplication;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.follow.FollowPO;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.follow.FollowRepositoryJpaVendor;
import cn.thoughtworks.school.pactise.diary.adapters.outbound.persistence.FlywayService;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

@Ignore
@SpringBootTest(classes = BackendApplication.class)
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner
@ActiveProfiles("test")
public class UserBase {
    @Autowired
    WebApplicationContext wac;

    @Autowired
    private FlywayService flywayService;

    @Autowired
    private FollowRepositoryJpaVendor followRepository;

    private static boolean cleanFlag = true;

    @Before
    public void setup() throws Exception {
        if (cleanFlag) {
            flywayService.migrateDatabase();
            FollowPO contactUser = new FollowPO((long) 1, (long) 2);
            followRepository.save(contactUser);
        }
        RestAssuredMockMvc.webAppContextSetup(wac);
    }
}
