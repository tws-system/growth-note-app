# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.28)
# Database: UserCenter
# Generation Time: 2019-12-12 06:56:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table emailReset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emailReset`;

CREATE TABLE `emailReset` (
  `uuid` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` bigint(11) NOT NULL,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('SUCCESS','PENDING','FAILURE') COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `emailReset` WRITE;
/*!40000 ALTER TABLE `emailReset` DISABLE KEYS */;

INSERT INTO `emailReset` (`uuid`, `userId`, `email`, `createTime`, `status`)
VALUES
	(X'3263393238303836363163303631363130313631633036393338643630303030',31,X'3135383239323538373337403136332E636F6D','2018-02-23 02:06:16',X'50454E44494E47'),
	(X'3263393238303836363164313135303030313631643131616131383130303030',31,X'3733343339383530314071712E636F6D','2018-02-26 07:53:58',X'4641494C555245'),
	(X'3263393238303836363164346630313630313631643466336266363030303030',31,X'3135383239323538373337403136332E636F6D','2018-02-27 01:49:59',X'53554343455353'),
	(X'3332393438373937366231653066383930313662326330313338303930303030',2660,X'313031343434393032394071712E636F6D','2019-06-06 08:56:45',X'50454E44494E47'),
	(X'3332393438626332366463653435653730313664656330306165393630303032',2996,X'3832313435333336364071712E636F6D','2019-10-21 01:48:50',X'50454E44494E47');

/*!40000 ALTER TABLE `emailReset` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table nodeBBUser
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nodeBBUser`;

CREATE TABLE `nodeBBUser` (
  `username` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` int(11) NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `fk_user_id_3` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `nodeBBUser` WRITE;
/*!40000 ALTER TABLE `nodeBBUser` DISABLE KEYS */;

INSERT INTO `nodeBBUser` (`username`, `password`, `userId`, `createDate`)
VALUES
	(X'41636579',X'313233343536',31,'2018-02-26 06:24:45'),
	(X'746573743131',X'5473326C674D66387525574074364C36',33,'2018-03-22 07:25:03');

/*!40000 ALTER TABLE `nodeBBUser` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderId` int(11) DEFAULT NULL,
  `receiverId` int(11) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `message` varchar(1000) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `createTime` date DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `message_en` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;

INSERT INTO `notification` (`id`, `senderId`, `receiverId`, `isRead`, `message`, `url`, `createTime`, `type`, `message_en`)
VALUES
	(1,1,1,1,' 成长日志中用户 李四 在 2018-07-26日 的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(2,1,5,1,' 成长日志中用户 李四 在优秀日志 admin(@admin)-2018-08-02日 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(3,1,1,1,' 成长日志中用户 李四 在优秀日志 admin(@admin)-2018-08-02日 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(5,1,2,0,' 成长日志中用户 李四 在优秀日志「admin(@admin)-2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(6,1,1,1,' 成长日志中用户 李四 在优秀日志「admin(@admin)-2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(7,1,1,1,' 成长日志中用户 李四 在优秀日志「李四(@11) - 2018-07-20日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(8,1,5,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(9,1,1,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(10,1,2,0,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(11,1,4,0,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(12,1,5,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(13,1,2,0,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(14,1,1,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(15,1,3,0,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(16,1,4,0,' 成长日志中用户 李四 在优秀日志「(@22) - 2018-07-20日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(17,1,1,1,' 成长日志中用户 李四 在优秀日志「(@22) - 2018-07-20日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(18,1,1,1,' 成长日志中用户 李四 在 2018-07-26日 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/practise-diaries','2018-08-22','GROWTH_LOG_TUTOR_TO_STUDENT',NULL),
	(19,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/practise-diaries','2018-08-22','GROWTH_LOG_TUTOR_TO_STUDENT',NULL),
	(20,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(21,1,1,1,' 成长日志中用户 李四 在优秀日志「李四(@11) - 2018-07-20日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(22,1,5,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(23,1,1,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(24,1,2,0,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(25,1,3,0,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(26,5,1,1,' 成长日志中学员 admin 在 2018-08-02日 的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(27,5,5,1,' 成长日志中学员 admin 在 2018-08-02日 的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(28,5,38,0,' 成长日志中学员 admin 在 2018-08-02日 的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(29,5,5,1,' 成长日志中助教 admin 在 2018-08-02日 的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-08-22','GROWTH_LOG_TUTOR_TO_STUDENT',NULL),
	(30,5,4,0,' 成长日志中助教 admin 在 2018-07-26日 的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-08-22','GROWTH_LOG_TUTOR_TO_STUDENT',NULL),
	(31,5,1,1,' 成长日志中助教 admin 在 2018-07-26日 的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-08-22','GROWTH_LOG_TUTOR_TO_STUDENT',NULL),
	(32,5,1,1,' 成长日志中助教 admin 在 2018-07-20日 的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-08-22','GROWTH_LOG_TUTOR_TO_STUDENT',NULL),
	(33,5,11,0,' 成长日志中助教 admin 在 2018-07-20日 的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-08-22','GROWTH_LOG_TUTOR_TO_STUDENT',NULL),
	(34,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 如何提问 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/2/assignment/59/quiz/4','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(35,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 如何写 Blog 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/25/assignment/69/quiz/17','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(36,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 Git 简单应用 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/27/assignment/71/quiz/19','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(37,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 Git 简单应用 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/27/assignment/71/quiz/19','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(38,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 Git 简单应用 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/27/assignment/71/quiz/19','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(39,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 Git 简单应用 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/27/assignment/71/quiz/19','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(40,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 Git 简单应用 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/27/assignment/71/quiz/19','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(41,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 Git 简单应用 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/27/assignment/71/quiz/19','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(42,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 如何提问 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/2/assignment/59/quiz/4','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(43,21,21,1,'JavaScript Pre Course 训练营的助教 张三批阅了你的 如何提问 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/2/assignment/59/quiz/4','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(44,1,5,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(45,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(46,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(47,1,5,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(48,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/practise-diaries','2018-08-22','GROWTH_LOG_TUTOR_TO_STUDENT',NULL),
	(49,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(50,1,5,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(51,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(52,1,5,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(53,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(54,1,5,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-22','GROWTH_LOG_STUDENT_TO_TUTOR',NULL),
	(55,1,5,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(56,1,11,0,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(57,1,2,0,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(58,1,1,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-22','GROWTH_EXCELLENT_LOG',NULL),
	(59,5,5,1,' MH测试训练营 训练营的助教 admin批阅了你的 单语言题目 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/student/program/58/task/53/assignment/355/quiz/:quizId','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(60,5,5,1,' MH测试训练营 训练营的助教 admin批阅了你的 单语言题目 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/student/program/58/task/53/assignment/355/quiz/:quizId','2018-08-22','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(61,5,4,0,' MH测试训练营 训练营的助教 admin关注了你!   Ta 的Email是：admin@admin.com 微信号是：weixin222 QQ号是：839487382','https://school.thoughtworks.cn/learn-staging/notification-center/index.html#/','2018-08-23','TUTOR_FOLLOW_STUDENT',NULL),
	(62,5,34,0,' MH测试训练营 训练营的助教 admin关注了你!   Ta 的Email是：admin@admin.com 微信号是：weixin222 QQ号是：839487382','https://school.thoughtworks.cn/learn-staging/notification-center/index.html#/','2018-08-23','TUTOR_FOLLOW_STUDENT',NULL),
	(63,5,5,1,' MH测试训练营 训练营的学员 admin完成了 单栈2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/58/task/59/student/5/divider/assignment/499/quiz/8','2018-08-23','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(64,5,31,1,'JavaScript pre courses 训练营的助教 admin批阅了你的 Markdown 的使用 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/1/task/5/assignment/230/quiz/40','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(65,5,31,1,'JavaScript pre courses 训练营的助教 admin批阅了你的 Markdown 的使用 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/1/task/5/assignment/230/quiz/40','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(66,5,1,1,' 单语言编程题测试 训练营的助教 admin对你的 编程题 作业进行了评论，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/student/program/59/task/47/assignment/385/quiz/27','2018-08-27','TUTOR_COMMENT_ASSIGNMENT',NULL),
	(67,5,32,0,'JavaScript pre courses 训练营的助教 admin批阅了你的 Markdown 的使用 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/1/task/5/assignment/230/quiz/40','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(68,5,1,1,'JavaScript pre courses 训练营的学员 admin完成了 markdown2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/1/task/46/student/5/divider/assignment/422/quiz/91','2018-08-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(69,5,5,1,'JavaScript pre courses 训练营的学员 admin完成了 markdown2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/1/task/46/student/5/divider/assignment/422/quiz/91','2018-08-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(70,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 如何提问 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/59/task/45/student/5/divider/assignment/344/quiz/39','2018-08-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(71,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/59/task/45/student/5/divider/assignment/398/quiz/27','2018-08-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(72,21,21,1,'JavaScript Pre Course 训练营的学员 张三完成了 初识 Linux 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/tutor/program/1/task/38/student/21/divider/assignment/102/quiz/44','2018-08-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(73,21,21,1,'JavaScript Pre Course 训练营的助教 张三对你的 教学方法 作业进行了评论，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/40/assignment/105/quiz/47','2018-08-27','TUTOR_COMMENT_ASSIGNMENT',NULL),
	(74,21,21,1,'第四期 JavaScript 全栈课程第一阶段 训练营的助教 张三对你的 黄焖鸡 作业进行了评论，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/46/task/194/assignment/406/quiz/97','2018-08-27','TUTOR_COMMENT_ASSIGNMENT',NULL),
	(75,21,21,1,'第四期 JavaScript 全栈课程第二阶段 训练营的助教 张三关注了你!   Ta 的Email是：mm@qq.com','http://localhost:3006/notification-center/index.html#/','2018-08-27','TUTOR_FOLLOW_STUDENT',NULL),
	(76,21,21,1,'第四期 JavaScript 全栈课程第二阶段 训练营的助教 张三对你的 OO step by step 作业进行了评论，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/47/task/203/assignment/417/quiz/110','2018-08-27','TUTOR_COMMENT_ASSIGNMENT',NULL),
	(77,21,21,1,'第四期 JavaScript 全栈课程第二阶段 训练营的助教 张三对你的 OO step by step 作业进行了评论，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/47/task/203/assignment/417/quiz/110','2018-08-27','TUTOR_COMMENT_ASSIGNMENT',NULL),
	(78,21,21,1,'第四期 JavaScript 全栈课程第二阶段 训练营的助教 张三对你的 OO step by step 作业进行了评论，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/47/task/203/assignment/417/quiz/91','2018-08-27','TUTOR_COMMENT_ASSIGNMENT',NULL),
	(79,21,21,1,'第四期 JavaScript 全栈课程第二阶段 训练营的学员 张三对 OO step by step 的作业进行了回复 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/tutor/program/47/task/203/student/21/divider/assignment/417/quiz/91','2018-08-27','STUDENT_COMMENT_ASSIGNMENT',NULL),
	(80,5,5,1,' MH测试训练营 训练营的学员 admin对 Markdown 的使用 的作业进行了回复 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/58/task/46/student/5/divider/assignment/346/quiz/40','2018-08-27','STUDENT_COMMENT_ASSIGNMENT',NULL),
	(81,5,1,1,' MH测试训练营 训练营的助教 admin批阅了你的 Markdown 的使用 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/346/quiz/40','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(82,5,1,1,' MH测试训练营 训练营的助教 admin批阅了你的 Markdown 的使用 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/346/quiz/40','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(83,5,1,1,' MH测试训练营 训练营的助教 admin批阅了你的 Markdown 的使用 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/346/quiz/40','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(84,5,1,1,' MH测试训练营 训练营的助教 admin批阅了你的 Markdown 的使用 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/346/quiz/40','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(85,5,1,1,' MH测试训练营 训练营的助教 admin批阅了你的 Markdown 的使用 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/346/quiz/40','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(86,5,5,1,' 单语言编程题测试 训练营的助教 admin关注了你!   Ta 的Email是：admin@admin.com 微信号是：weixin222 QQ号是：839487382','https://school.thoughtworks.cn/learn-staging/notification-center/index.html#/','2018-08-27','TUTOR_FOLLOW_STUDENT',NULL),
	(87,5,5,1,' 单语言编程题测试 训练营的助教 admin批阅了你的 编程题 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/student/program/59/task/47/assignment/385/quiz/27','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(88,5,5,1,' 单语言编程题测试 训练营的助教 admin批阅了你的 编程题 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/student/program/59/task/47/assignment/385/quiz/27','2018-08-27','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(89,5,5,1,' 单语言编程题测试 训练营的助教 admin对你的 编程题 作业进行了评论，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/student/program/59/task/47/assignment/385/quiz/27','2018-08-27','TUTOR_COMMENT_ASSIGNMENT',NULL),
	(90,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 编程题 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/59/task/47/student/5/divider/assignment/446/quiz/24','2018-08-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(91,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 编程题 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/59/task/47/student/5/divider/assignment/446/quiz/24','2018-08-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(92,5,5,1,' MH测试训练营 训练营的学员 admin完成了 单栈3 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/58/task/59/student/5/divider/assignment/506/quiz/8','2018-08-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(93,1,5,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-28','GROWTH_EXCELLENT_NOTE',NULL),
	(94,1,1,1,' 成长日志中用户 李四 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-28','GROWTH_EXCELLENT_NOTE',NULL),
	(95,5,5,1,' Ma Huan Huan测试训练营 训练营的学员 admin对 主观题1 的作业进行了回复 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/58/task/46/student/5/divider/assignment/346/quiz/40','2018-08-28','STUDENT_COMMENT_ASSIGNMENT',NULL),
	(96,5,5,1,' Ma Huan Huan测试训练营 训练营的学员 admin对 主观题1 的作业进行了回复 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/58/task/46/student/5/divider/assignment/346/quiz/40','2018-08-28','STUDENT_COMMENT_ASSIGNMENT',NULL),
	(97,5,5,1,' Ma Huan Huan测试训练营 训练营的学员 admin完成了 主观题3 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/58/task/46/student/5/divider/assignment/519/quiz/86','2018-08-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(98,5,5,1,' Ma Huan Huan测试训练营 训练营的学员 admin完成了 单栈4 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/58/task/59/student/5/divider/assignment/507/quiz/7','2018-08-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(99,5,5,1,' Ma Huan Huan测试训练营 训练营的学员 admin完成了 编程题目4 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/58/task/71/student/5/divider/assignment/522/quiz/32','2018-08-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(100,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 主观题2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/422/quiz/91','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(101,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 主观题2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/422/quiz/91','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(102,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 主观题2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/422/quiz/91','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(103,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 主观题2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/422/quiz/91','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(104,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 主观题2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/422/quiz/91','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(105,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 主观题2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/422/quiz/91','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(106,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 单栈2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/student/program/58/task/59/assignment/499/quiz/8','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(107,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 单栈2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/student/program/58/task/59/assignment/499/quiz/8','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(108,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 编程题目4 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/student/program/58/task/71/assignment/522/quiz/32','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(109,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 编程题目4 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/student/program/58/task/71/assignment/522/quiz/32','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(110,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 编程题目4 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/student/program/58/task/71/assignment/522/quiz/32','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(111,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 编程题目4 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/student/program/58/task/71/assignment/522/quiz/32','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(112,5,5,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 主观题3 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/519/quiz/86','2018-08-28','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(113,1,5,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-28','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(114,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-28','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(115,5,5,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-28','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(116,5,1,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-28','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(117,5,5,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-08-28','GROWTH_NOTE_TUTOR_TO_STUDENT',NULL),
	(118,5,5,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-08-28','GROWTH_NOTE_TUTOR_TO_STUDENT',NULL),
	(119,5,5,1,' 成长日志中用户 admin 在优秀日志「admin(@admin) - 2018-08-02日」 的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/excellent-diaries','2018-08-28','GROWTH_EXCELLENT_NOTE',NULL),
	(120,1,5,1,' 成长日志中用户 李四 在优秀日志「admin(@) - 2018-02-06日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-30','GROWTH_EXCELLENT_NOTE',NULL),
	(121,1,1,1,' 成长日志中用户 李四 在优秀日志「admin(@) - 2018-02-06日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-30','GROWTH_EXCELLENT_NOTE',NULL),
	(122,1,2,0,' 成长日志中用户 李四 在优秀日志「admin(@) - 2018-02-06日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-30','GROWTH_EXCELLENT_NOTE',NULL),
	(123,1,1,1,' 成长日志中用户 李四 在优秀日志「李四(@) - 2018-07-26日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-30','GROWTH_EXCELLENT_NOTE',NULL),
	(124,1,2,0,' 成长日志中用户 李四 在优秀日志「李四(@) - 2018-07-26日」 的日志中评论了你! ','http://localhost:3006/practise-diary/index.html#/excellent-diaries','2018-08-30','GROWTH_EXCELLENT_NOTE',NULL),
	(125,1,1,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-30','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(126,1,5,1,' 成长日志中用户 李四 在「2018-07-26日」的日志中回复了你! ','http://localhost:3006/practise-diary/index.html#/followees/1','2018-08-30','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(127,5,5,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-30','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(128,5,5,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-30','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(129,5,1,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-30','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(130,5,5,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-08-30','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(131,5,5,1,' 成长日志中用户 admin 在「2018-08-28日」的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-08-30','GROWTH_NOTE_TUTOR_TO_STUDENT',NULL),
	(132,5,5,1,' 成长日志中用户 admin 在优秀日志「admin(@) - 2018-02-06日」 的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/excellent-diaries','2018-09-04','GROWTH_EXCELLENT_NOTE',NULL),
	(133,5,5,1,' 成长日志中用户 admin 在优秀日志「admin(@) - 2018-02-06日」 的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/excellent-diaries','2018-09-05','GROWTH_EXCELLENT_NOTE',NULL),
	(134,5,1,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 主观题1 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/58/task/46/assignment/346/quiz/40','2018-09-05','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(135,21,11,0,'JavaScript Pre Course 训练营的助教 张三批阅了你的 如何提问 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/student/program/1/task/2/assignment/59/quiz/4','2018-09-05','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(136,1,5,1,' Ma Huan Huan测试训练营 训练营的学员 李四完成了 单栈2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/58/task/59/student/1/divider/assignment/499/quiz/8','2018-09-05','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(137,5,1,1,' Ma Huan Huan测试训练营 训练营的助教 admin批阅了你的 单栈2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/student/program/58/task/59/assignment/499/quiz/8','2018-09-05','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(138,1,5,1,' Ma Huan Huan测试训练营 训练营的学员 李四完成了 单栈2 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/58/task/59/student/1/divider/assignment/499/quiz/8','2018-09-05','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(139,5,5,1,' 成长日志中用户 admin 在「2018-09-26日」的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/practise-diaries','2018-09-06','GROWTH_NOTE_TUTOR_TO_STUDENT',NULL),
	(140,5,4,0,' 成长日志中用户 admin 在优秀日志「(@) - 2018-07-20日」 的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/excellent-diaries','2018-09-06','GROWTH_EXCELLENT_NOTE',NULL),
	(141,5,5,1,' 成长日志中用户 admin 在「2018-09-26日」的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-09-06','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(142,5,5,1,' 成长日志中用户 admin 在优秀日志「admin(@) - 2018-02-06日」 的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/excellent-diaries','2018-09-06','GROWTH_EXCELLENT_NOTE',NULL),
	(143,5,5,1,' xi sin 测试专用 训练营的学员 admin完成了 111 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/60/task/75/student/5/divider/assignment/526/quiz/90','2018-09-07','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(144,5,5,1,' xi sin 测试专用 训练营的助教 admin批阅了你的 111 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/student/program/60/task/75/assignment/526/quiz/90','2018-09-07','TUTOR_MODIFY_ASSIGNMENT_STATUS',NULL),
	(145,5,5,1,' 成长日志中用户 admin 在「2018-09-27日」的日志中回复了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/followees/5','2018-09-07','GROWTH_NOTE_STUDENT_TO_TUTOR',NULL),
	(146,21,21,1,'第四期 JavaScript 全栈课程第一阶段 训练营的学员 张三完成了 JavaScript-集合运算合集 作业 ，快去看看吧！ ','http://localhost:3006/program-center/homeworkQuiz/index.html#/tutor/program/46/task/193/student/21/divider/assignment/405/quiz/39','2018-09-11','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(147,21,21,1,'第四期 JavaScript 全栈课程第一阶段 训练营的学员 张三完成了 JavaScript-集合运算合集 作业 ，快去看看吧！ ','http://localhost:3006/program-center/homeworkQuiz/index.html#/tutor/program/46/task/193/student/21/divider/assignment/405/quiz/39','2018-09-11','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(148,21,21,1,'第四期 JavaScript 全栈课程第一阶段 训练营的学员 张三完成了 JavaScript-集合运算合集 作业 ，快去看看吧！ ','http://localhost:3006/program-center/homeworkQuiz/index.html#/tutor/program/46/task/193/student/21/divider/assignment/405/quiz/39','2018-09-11','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(149,21,21,0,'JavaScript Pre Course 训练营的学员 张三完成了 教学方法 作业 ，快去看看吧！ ','http://localhost:3006/program-center/subjectiveQuiz/index.html#/tutor/program/1/task/40/student/21/divider/assignment/105/quiz/91','2018-09-12','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(150,5,1,1,' 成长日志中用户 admin 在优秀日志「李四(@11) - 2018-07-29日」 的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/excellent-diaries','2018-09-13','GROWTH_EXCELLENT_NOTE',NULL),
	(151,5,5,1,' 成长日志中用户 admin 在优秀日志「李四(@11) - 2018-07-29日」 的日志中评论了你! ','https://school.thoughtworks.cn/learn-staging/practise-diary/index.html#/excellent-diaries','2018-09-13','GROWTH_EXCELLENT_NOTE',NULL),
	(152,5,5,1,' Ma Huan Huan测试训练营 训练营的学员 admin对 主观题1 的作业进行了回复 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/subjectiveQuiz/index.html#/tutor/program/58/task/46/student/5/divider/assignment/346/quiz/40','2018-09-19','STUDENT_COMMENT_ASSIGNMENT',NULL),
	(153,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(154,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(155,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(156,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(157,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(158,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(159,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(160,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(161,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(162,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(163,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(164,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(165,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(166,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(167,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(168,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/10','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(169,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(170,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-26','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(171,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(172,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(173,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(174,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(175,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(176,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(177,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(178,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 null 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/333/quiz/7','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(179,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(180,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(181,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(182,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(183,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(184,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(185,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(186,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(187,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(188,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(189,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(190,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(191,5,38,0,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(192,5,5,1,' 单语言编程题测试 训练营的学员 admin完成了 数组求和 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/onlineLanguageQuiz/index.html#/tutor/program/59/task/34/student/5/divider/assignment/530/quiz/11','2018-09-27','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(193,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/449/quiz/32','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(194,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/449/quiz/32','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(195,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/449/quiz/32','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(196,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/449/quiz/32','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(197,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(198,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(199,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(200,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(201,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(202,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(203,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(204,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(205,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(206,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(207,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(208,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(209,5,5,1,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL),
	(210,5,4,0,' 实验室纳新题 训练营的学员 admin完成了 java测试 作业 ，快去看看吧！ ','https://school.thoughtworks.cn/learn-staging/program-center/homeworkQuiz/index.html#/tutor/program/4/task/14/student/5/divider/assignment/529/quiz/33','2018-09-28','STUDENT_MODIFY_ASSIGNMENT_STATUS',NULL);

/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table passwordReset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `passwordReset`;

CREATE TABLE `passwordReset` (
  `uuid` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` int(11) NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isVaild` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `passwordReset` WRITE;
/*!40000 ALTER TABLE `passwordReset` DISABLE KEYS */;

INSERT INTO `passwordReset` (`uuid`, `userId`, `createTime`, `isVaild`)
VALUES
	(X'3263393238303833356633326431336230313566333264336638313330303030',2,'2017-10-19 04:11:13',X'30'),
	(X'3263393238303833356633336631313330313566333366376431343830303030',2,'2017-10-19 09:29:59',X'30'),
	(X'3263393238303833363033353161363430313630333531643638386230303030',30,'2017-12-08 07:53:27',X'31'),
	(X'3263393238303836363135306533663530313631353065633633393430303032',2,'2018-02-01 10:32:04',X'31'),
	(X'3263393238303836363135343364356130313631353433663135323830303030',2,'2018-02-02 02:01:15',X'31'),
	(X'3332393438316138363663343539373730313636633436316433313330303031',33,'2018-10-30 09:50:37',X'30'),
	(X'3332393438316138363663343539373730313636633438306130363330303033',33,'2018-10-30 10:24:15',X'31'),
	(X'3332393438316138363663343539373730313636633438316332656530303034',5,'2018-10-30 10:25:30',X'31'),
	(X'3332393438316138363663343539373730313636633438343037613830303035',31,'2018-10-30 10:27:59',X'31'),
	(X'3332393438316138363663343539373730313636633438393634383230303036',38,'2018-10-30 10:33:50',X'31'),
	(X'3332393438316661363730373630323730313637313163386161346630303030',33,'2018-11-14 10:33:42',X'30'),
	(X'3332393439316130363937613264393430313639376133303763646530303030',5,'2019-03-14 03:13:11',X'30');

/*!40000 ALTER TABLE `passwordReset` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table schema_version
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schema_version`;

CREATE TABLE `schema_version` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `schema_version_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `schema_version` WRITE;
/*!40000 ALTER TABLE `schema_version` DISABLE KEYS */;

INSERT INTO `schema_version` (`installed_rank`, `version`, `description`, `type`, `script`, `checksum`, `installed_by`, `installed_on`, `execution_time`, `success`)
VALUES
	(1,'1','Create user table structure','SQL','V1__Create_user_table_structure.sql',1165008760,'UserCenter','2017-09-21 03:18:02',19,1),
	(2,'2','Create user role table structure','SQL','V2__Create_user_role_table_structure.sql',-1876400556,'UserCenter','2017-09-21 03:18:02',14,1),
	(3,'3','Create user program table structure','SQL','V3__Create_user_program_table_structure.sql',2118555716,'UserCenter','2017-09-21 03:18:02',12,1),
	(4,'4','Create nodebb user table structure','SQL','V4__Create_nodebb_user_table_structure.sql',1392439081,'UserCenter','2017-09-21 03:18:02',12,1),
	(5,'5','Add constraint for user table','SQL','V5__Add_constraint_for_user_table.sql',-741293180,'UserCenter','2017-09-21 03:18:02',42,1),
	(6,'6','Modify user table change createDate','SQL','V6__Modify_user_table_change_createDate.sql',191064758,'UserCenter','2017-09-21 03:18:02',71,1),
	(7,'7','Modify nodebb user table change createDate','SQL','V7__Modify_nodebb_user_table_change_createDate.sql',-1540214605,'UserCenter','2017-09-21 03:18:02',27,1),
	(8,'8','Create user detail table','SQL','V8__Create_user_detail_table.sql',641458765,'UserCenter','2017-09-21 03:18:02',14,1),
	(9,'9','Create user role table','SQL','V9__Create_user_role_table.sql',115329669,'UserCenter','2017-09-21 03:18:02',22,1),
	(10,'10','Add-constraint for nodebb user','SQL','V10__Add-constraint_for_nodebb_user.sql',853900043,'UserCenter','2017-09-21 03:18:02',58,1),
	(11,'11','Add-id in user role','SQL','V11__Add-id_in_user_role.sql',-1607978930,'UserCenter','2017-10-19 03:50:56',87,1),
	(12,'12','Create password reset table structure','SQL','V12__Create_password_reset_table_structure.sql',-1850379588,'UserCenter','2017-10-19 03:50:56',12,1),
	(13,'13','Create email reset table structure','SQL','V13__Create_email_reset_table_structure.sql',-2112067276,'UserCenter','2018-02-07 06:14:26',390,1),
	(14,'14','Alter email reset table structure','SQL','V14__Alter_email_reset_colmun.sql',-39302454,'UserCenter','2018-02-07 14:39:51',111,1),
	(15,'15','Add wechat and qq columns in user detail table','SQL','V15__Add_wechat_and_qq_columns_in_user_detail_table.sql',-1545865221,'UserCenter','2018-08-06 02:49:02',351,1),
	(16,'16','create notification table','SQL','V16__create_notification_table.sql',-799132571,'UserCenter','2018-08-22 03:00:10',24,1),
	(17,'17','divide designer role','SQL','V17__divide_designer_role.sql',-113603056,'UserCenter','2018-10-23 09:43:00',167,1),
	(18,'18','Create social account table','SQL','V18__Create_social_account_table.sql',-1258457130,'UserCenter','2018-10-26 09:50:20',101,1),
	(19,'19','add areaCode column in user table','SQL','V19__add_areaCode_column_in_user_table.sql',-659739800,'UserCenter','2018-10-31 01:54:29',243,1),
	(20,'20','add message en column in notification table','SQL','V20__add_message_en_column_in_notification_table.sql',-1395916527,'UserCenter','2018-11-02 05:51:40',77,1),
	(21,'21','migrate wecaht info to social account table','SQL','V21__migrate_wecaht_info_to_social_account_table.sql',265789270,'UserCenter','2018-11-02 19:38:42',32,1),
	(22,'22','Alter userDetail table structure','SQL','V22__Alter_userDetail_table_structure.sql',-858811615,'UserCenter','2018-11-06 07:29:58',173,1),
	(23,'23','add  organizationId in userRole table','SQL','V23__add__organizationId_in_userRole_table.sql',-349330836,'UserCenter','2019-02-13 06:35:03',98,1),
	(24,'24','add  currentOrganizationId in userDetail table','SQL','V24__add__currentOrganizationId_in_userDetail_table.sql',357193054,'UserCenter','2019-02-13 06:35:03',61,1),
	(25,'25','delete user role unique','SQL','V25__delete_user_role_unique.sql',-568148029,'UserCenter','2019-02-14 09:42:33',34,1);

/*!40000 ALTER TABLE `schema_version` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table socialAccount
# ------------------------------------------------------------

DROP TABLE IF EXISTS `socialAccount`;

CREATE TABLE `socialAccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(128) COLLATE utf8_bin NOT NULL,
  `displayName` varchar(128) COLLATE utf8_bin NOT NULL,
  `openid` varchar(128) COLLATE utf8_bin NOT NULL,
  `unionid` varchar(128) COLLATE utf8_bin NOT NULL,
  `imageUrl` varchar(256) COLLATE utf8_bin NOT NULL,
  `accessToken` varchar(256) COLLATE utf8_bin NOT NULL,
  `refreshToken` varchar(256) COLLATE utf8_bin NOT NULL,
  `expireTime` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `socialAccount` WRITE;
/*!40000 ALTER TABLE `socialAccount` DISABLE KEYS */;

INSERT INTO `socialAccount` (`id`, `username`, `type`, `displayName`, `openid`, `unionid`, `imageUrl`, `accessToken`, `refreshToken`, `expireTime`)
VALUES
	(11,X'31313131',X'77656978696E',X'313131',X'6F366D6D307755413948325A70504F6D56352D665A5546774D34674D',X'',X'687474703A2F2F746869726477782E716C6F676F2E636E2F6D6D6F70656E2F76695F33322F445941494F677138336571586D6D70696279675A69637A6D614738565049596D4B447042456B73556430387149614D344B6369616B675A4D5633714130664C444A6873426467387074474A6371796C62696155366D78353274672F313332',X'31345F7541692D4B51326C62466B4B36343155365571744B776B516C4537335343347433736F6E474B7A356A546568626D476D733041743165793678477135512D4C51724C5844367369734B49327A6365316E784770656641',X'31345F5A43707745684B48694A4A37546678657A37494C3148505F502D7A666348535059543461424D664E5661355A3443445675546E65704979633164525168596679315F63535236364B63306E787277754976384F723951',2147483647),
	(12,X'78787A757265',X'77656978696E',X'58587A757265',X'6F366D6D307765775450427634316F3245314362623837644E493030',X'6F6731697476795168356446774D356D635979746A743158387A5A51',X'687474703A2F2F746869726477782E716C6F676F2E636E2F6D6D6F70656E2F76695F33322F51306A3454774754665449765775415261594A564E65515772526963514B326434314E69626857675776565A71597937346A6A6E6672364759663036334C506F71787147756961373747457A514C62627472674D737A6E41412F313332',X'32315F76515A33664C4B364C72523073374D5756624257785463465A5671464B4C7163683864597944666F55584F5F544B5A4D6A57656E546D734169695F7279663071595A384D5336546F3766596B47376C54527843774F577535575A7731676F4355705F6748696E493067764D',X'32315F4A6D746A485F6572713633556B35396F437156677939655277636B4A3354365A745842586C785A5959537746567A59644955553863724B425433674741784B516E5161685732707348684E2D576C4B44455A5068307249554953546C49677A7755474A50364D4B6E773763',7200);

/*!40000 ALTER TABLE `socialAccount` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `mobilePhone` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `areaCode` int(11) DEFAULT '86',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `userName` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `email`, `mobilePhone`, `password`, `userName`, `createDate`, `areaCode`)
VALUES
	(1,X'3140312E636F6D',X'3139393230323030383635',X'6563366136353336636133303465646638343464316432343861346630386463',X'3131','2017-09-27 07:03:19',86),
	(2,X'313032353338333134334071712E636F6D',X'3138383239323930333232',X'3535306531626166653037376666306230623637663465333266323964373531',X'6868','2017-09-27 08:01:02',86),
	(3,X'6840682E636F6D',X'3138383239323930333137',X'6539393431356339336136376431636636396631333733663831343833303865',X'7676','2017-09-27 08:01:32',86),
	(4,X'3240322E636F6D',X'34323334333435',X'3535306531626166653037376666306230623637663465333266323964373531',X'3232','2017-09-27 09:53:05',86),
	(5,X'616365797378403136332E636F6D',X'3133323638383033323231',X'3535306531626166653037376666306230623637663465333266323964373531',X'61646D696E','2017-09-28 07:03:19',86),
	(11,X'3131314071712E636F6D',X'3133323131313131313131',X'3535306531626166653037376666306230623637663465333266323964373531',X'68656C6C6F','2018-06-05 02:15:36',86),
	(21,X'6D6D4071712E636F6D',X'3139393230323030383635',X'3535306531626166653037376666306230623637663465333266323964373531',X'E5BCA0E4B889','2017-11-22 06:53:59',86),
	(26,X'3130303239323930333232',X'3138383230323037363433',X'3535306531626166653037376666306230623637663465333266323964373531',X'E7A69B','2017-11-15 03:09:35',86),
	(27,X'31314071712E636F6D',X'3138383239323930303030',X'3535306531626166653037376666306230623637663465333266323964373531',X'64656D6F323232','2017-11-20 07:08:02',86),
	(29,X'313636393634373733374071712E636F6D',X'3138323731393239363333',X'3939313335636364323863326461376232346638323064376235393662663739',X'6A6C','2017-12-07 08:00:47',86),
	(30,X'686D614074686F75677468776F726B732E636F6D',X'3138383239323930333434',X'3535306531626166653037376666306230623637663465333266323964373531',X'4D48776973686573','2017-12-08 01:06:41',86),
	(31,X'3135383239323538373337403136332E636F6D',X'3137383638383033323635',X'3535306531626166653037376666306230623637663465333266323964373531',X'64656D6F','2017-12-08 08:05:23',86),
	(32,X'313233313233314071712E636F6D',X'313233313233313233',X'3535306531626166653037376666306230623637663465333266323964373531',X'7465737431','2017-12-20 09:47:17',86),
	(33,X'3734383839303234374071712E636F6D',X'3133383638383033323635',X'3134653162363030623166643537396634373433336238386538643835323931',X'746573743131','2018-03-22 07:25:01',86),
	(34,X'38323134353333363631314071712E636F6D',X'3135383032393533373239',X'6634326431326330653533646139616434636531656661356439343333633266',X'343434','2018-05-18 06:48:01',86),
	(35,X'6D6C773631393840676D61696C2E636F6D',X'3135323931313934323734',X'6634326431326330653533646139616434636531656661356439343333633266',X'333333','2018-05-18 06:52:55',86),
	(38,X'7465737440656D61696C2E636F6D',X'3133363238313935363437',X'3535306531626166653037376666306230623637663465333266323964373531',X'746573746572','2018-07-02 02:29:30',86);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table UserConnection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `UserConnection`;

CREATE TABLE `UserConnection` (
  `userId` varchar(255) NOT NULL,
  `providerId` varchar(255) NOT NULL,
  `providerUserId` varchar(255) NOT NULL,
  `rank` int(11) NOT NULL,
  `displayName` varchar(255) DEFAULT NULL,
  `profileUrl` varchar(512) DEFAULT NULL,
  `imageUrl` varchar(512) DEFAULT NULL,
  `accessToken` varchar(512) NOT NULL,
  `secret` varchar(512) DEFAULT NULL,
  `refreshToken` varchar(512) DEFAULT NULL,
  `expireTime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`userId`,`providerId`,`providerUserId`),
  UNIQUE KEY `UserConnectionRank` (`userId`,`providerId`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `UserConnection` WRITE;
/*!40000 ALTER TABLE `UserConnection` DISABLE KEYS */;

INSERT INTO `UserConnection` (`userId`, `providerId`, `providerUserId`, `rank`, `displayName`, `profileUrl`, `imageUrl`, `accessToken`, `secret`, `refreshToken`, `expireTime`)
VALUES
	('admin','weixin','o6mm0wc2AefV3etxBYfD2WIJTsvA',0,NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqXmmpibygZiczmaG8VPIYmKDpBEksUd08qIaM4KciakgZMV3qA0fLDJhsBdg8ptGJcqylbiaU6mx52tg/132','15__wGsYHvzE8gNHMICCKAfaLwtT30CPRyaUXB1-vezkBgn0MdDwfUv2c_6aQiT9mgxV4tBIIKaUZhnNFK_AtNVWQ',NULL,'15_yGt7XobxpghxPJ_joqsNOLYRQEwYeWQrb50uFSmgGCzwsaV1_FOqmF22UCk4AhvnkWh-vjhvssRAq-jnopBBzg',NULL),
	('hello11','weixin','o6mm0wUA9H2ZpPOmV5-fZUFwM4gM',1,'hello11',NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqXmmpibygZiczmaG8VPIYmKDpBEksUd08qIaM4KciakgZMV3qA0fLDJhsBdg8ptGJcqylbiaU6mx52tg/132','14_uAi-KQ2lbFkK641U6UqtKwkQlE73SC4t3sonGKz5jTehbmGms0At1ey6xGq5Q-LQrLXD6sisKI2zce1nxGpefA',NULL,'14_ZCpwEhKHiJJ7Tfxez7IL1HP_P-zfcHSPYT4aBMfNVa5Z4CDVuTnepIyc1dRQhYfy1_cSR66Kc0nxrwuIv8Or9Q',1539862184586);

/*!40000 ALTER TABLE `UserConnection` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userDetail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userDetail`;

CREATE TABLE `userDetail` (
  `userId` int(11) NOT NULL,
  `school` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `major` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `degree` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `gender` enum('F','M') COLLATE utf8_bin DEFAULT NULL,
  `schoolProvince` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `schoolCity` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `entranceYear` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `wechat` varchar(50) COLLATE utf8_bin DEFAULT '',
  `qq` varchar(50) COLLATE utf8_bin DEFAULT '',
  `currentOrganizationId` int(11) DEFAULT '1',
  UNIQUE KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `userDetail` WRITE;
/*!40000 ALTER TABLE `userDetail` DISABLE KEYS */;

INSERT INTO `userDetail` (`userId`, `school`, `name`, `major`, `degree`, `gender`, `schoolProvince`, `schoolCity`, `entranceYear`, `wechat`, `qq`, `currentOrganizationId`)
VALUES
	(1,X'313939',X'E5BCA0E4B889',X'E8BDAFE4BBB6',X'E4B893E7A791',X'4D',X'E5A4A9E6B4A5E5B882',X'E5B882E8BE96E58CBA',X'323031382D31312D30325430373A32373A35332E3932365A',X'',X'',1),
	(2,X'E8A5BFE58C97E5A4A7E5ADA6',X'E5B08FE6988E',X'E8BDAFE4BBB6E5B7A5E7A88B',X'E69CACE7A791',X'4D',X'E99995E8A5BFE79C81',X'E8A5BFE5AE89E5B882',X'323031352D30392D31335430313A34303A35372E3634385A',X'',X'',1),
	(3,NULL,X'',NULL,NULL,NULL,NULL,NULL,NULL,X'',X'',1),
	(4,NULL,X'',NULL,NULL,NULL,NULL,NULL,NULL,X'',X'',1),
	(5,X'E8A5BFE58C97E5A4A7E5ADA6',X'E7AEA1E79086E59198',X'E8BDAFE4BBB6E5B7A5E7A88B',X'E7A094E7A9B6E7949F',X'4D',X'E4B8ADE59BBDE58FB0E6B9BE',NULL,X'323031392D31312D32345430313A33363A33382E3035345A',X'77656978696E32323231',X'383339343837333832',1),
	(11,X'313131',X'E883A4E7A69B',X'313131',X'E58D9AE5A3AB',X'46',X'E5B1B1E8A5BFE79C81',X'E5A4A7E5908CE5B882',X'323031392D30312D32325430313A32313A35352E3837312B30303030',X'',X'',1),
	(21,X'31',X'E5BCA0E4B88931',X'E8BDAFE4BBB6',X'E4B893E7A791',X'4D',X'E5A4A9E6B4A5E5B882',X'E5B882E8BE96E58CBA',X'323031382D31312D30325430373A32373A35332E3932365A',X'',X'',4),
	(26,X'',X'E6B19FE8BEB0',X'E58F91E5A49AE5B091',X'E587BAE78EB0',X'4D',X'E783ADE6B0B4E8A28B',X'E58F91E7949FE79A84',X'E998BFE696AFE89282E88AAC',X'',X'',1),
	(27,X'E8A5BFE794B5',X'E5B08FE6988E',X'E8BDAFE4BBB6',X'E69CACE7A791',X'46',X'E6B2B3E58C97E79C81',X'E59490E5B1B1E5B882',X'323031382D31312D30355430383A31383A35322E3638375A',X'',X'',1),
	(29,NULL,X'',NULL,NULL,NULL,NULL,NULL,NULL,X'',X'',1),
	(30,X'E8A5BFE58C97E5A4A7E5ADA6',X'E5B08FE6988E',X'E8BDAFE4BBB6E5B7A5E7A88B',X'E69CACE7A791',X'4D',X'E99995E8A5BFE79C81',X'E8A5BFE5AE89E5B882',X'323031352D30392D31335430313A34303A35372E3634385A',X'',X'',1),
	(31,X'32333233',X'6466',X'71',X'E69CACE7A791',X'4D',X'E58C97E4BAACE5B882',X'E5B882E8BE96E58CBA',X'323031382D30312D31355430373A32333A32362E3931365A',X'',X'',1),
	(32,X'313233',X'74657374',X'313233',X'E69CACE7A791',X'4D',X'E5A4A9E6B4A5E5B882',X'E5B882E8BE96E58CBA',X'323031372D31322D32305430393A35323A31312E3334385A',X'',X'',1),
	(33,X'6164',X'617364',X'617364',X'E69CACE7A791',X'4D',X'E58685E89299E58FA4E887AAE6B2BBE58CBA',X'E8B5A4E5B3B0E5B882',X'323031382D30332D32325430373A32383A31382E3130355A',X'',X'',1),
	(34,X'E8A5BFE58C97E5A4A7E5ADA6',X'E7AEA1E79086E5919854657374',X'E8BDAFE4BBB6E5B7A5E7A88B',X'E69CACE7A791',X'4D',X'756E646566696E6564',X'756E646566696E6564',X'323031382D30352D31385430363A34393A32342E3830355A',X'',X'',1),
	(35,X'E8A5BFE58C97E5A4A7E5ADA6',X'E5ADA6E59198E7ABAF54657374',X'E8BDAFE4BBB6E5B7A5E7A88B',X'E69CACE7A791',X'4D',X'756E646566696E6564',X'756E646566696E6564',X'323031382D30352D31385430363A35333A34382E3736365A',X'',X'',1),
	(38,X'E69F90E5A4A7E5ADA6',X'E6B58BE8AF95',X'E69F90E4B893E4B89A',X'E4B893E7A791',X'4D',X'E58685E89299E58FA4E887AAE6B2BBE58CBA',X'E4B98CE6B5B7E5B882',X'323031382D30372D30325430333A33373A31342E3034325A',X'',X'',1);

/*!40000 ALTER TABLE `userDetail` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userProgram
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userProgram`;

CREATE TABLE `userProgram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `mobilePhone` varchar(64) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `createDate` int(11) NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table userRole
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userRole`;

CREATE TABLE `userRole` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `role` int(7) NOT NULL,
  `organizationId` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `userRole` WRITE;
/*!40000 ALTER TABLE `userRole` DISABLE KEYS */;

INSERT INTO `userRole` (`id`, `userId`, `role`, `organizationId`)
VALUES
	(7,21,2,1),
	(37,3,5,1),
	(85,35,2,1),
	(88,5,2,1),
	(357,1,2,1),
	(358,1,5,1);

/*!40000 ALTER TABLE `userRole` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `mobilePhone` varchar(64) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `createDate` int(11) NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `mobilePhone`, `password`, `createDate`, `userName`)
VALUES
	(1,X'61646D696E4061646D696E2E636F6D',X'3138373131313130303030',X'3439623036643861626431663735306637333832363562306131653666313931',1400000000,X'61646D696E'),
	(3,X'32324071712E636F6D',X'3138373131313130303030',X'3762363763616336333232353865333738313133333234316465303533343838',1493013757,X'6C6561726E74657374'),
	(4,X'716D6D4071712E636F6D',X'3138373131313130303030',X'6636346436383830636437363963353361626537383162613039623566333562',1493014152,X'716D6D'),
	(5,X'6368656E6740312E636F6D',X'3138373131313130303030',X'3037633338363330326334383830613531343337613838353639363331653031',1493014365,X'6368656E67'),
	(6,X'71714071712E636F6D',X'3138373131313130303030',X'6563646365613732363535333866646236333838323434626262636239623730',1493014786,X'7171'),
	(7,X'3140312E636F6D',X'3138373131313130303030',X'3862366534383933346331373239363231626564613061326364376130376466',1493017005,X'6173646667686A6B'),
	(8,X'6473614035362E647361',X'3138373131313130303030',X'6536306164323936663135373131623966383636393933313261663932333130',1493017357,X'7364616864676A'),
	(9,X'6368656E674071712E636F6D',X'3138373131313130303030',X'3037633338363330326334383830613531343337613838353639363331653031',1493018866,X'786663677668626A6E'),
	(10,X'6766676667406466672E76626E',X'3138373131313130303030',X'3334336236343261363137636434323464656538323735363735613935326162',1493029464,X'E59CB0E696B9E8A784E58892E5B180'),
	(11,X'3334393838343032324071712E636F6D',X'3138373131313130303030',X'6132313338313365343832393964623832636163613436363231353036646161',1493107678,X'6265416D'),
	(13,X'6166614071712E636F6D',X'3138373131313130303030',X'3037633338363330326334383830613531343337613838353639363331653031',1493172082,X'747731'),
	(14,X'77795F6231393935403136332E636F6D',X'3138373131313130303030',X'3235643535616432383361613430306166343634633736643731336330376164',1493178026,X'777962'),
	(15,X'313734343535303335334071712E636F6D',X'3138373131313130303030',X'3439383564643861626466373461616632633838376535323866356236303936',1493186202,X'6C6F6E657266'),
	(16,X'64736164406461642E646173',X'3138373131313130303030',X'3235643535616432383361613430306166343634633736643731336330376164',1493190618,X'6166'),
	(17,X'3838384071712E636F6D',X'3138373131313130303030',X'3535306531626166653037376666306230623637663465333266323964373531',1493192022,X'79616E7A69'),
	(18,X'363636403636362E636F6D',X'3138373131313130303030',X'3535306531626166653037376666306230623637663465333266323964373531',1493192062,X'786975'),
	(19,X'6D61686F6E674071712E636F6D',X'3138373131313130303030',X'6335333731663164376130336364346234343038356433616461373039326466',1493193098,X'6D61686F6E67'),
	(20,X'6C63784071712E636F6D',X'3138373131313130303030',X'3235643535616432383361613430306166343634633736643731336330376164',1493198522,X'6C6378'),
	(21,X'63677662406466672E76626E',X'3138373131313130303030',X'3938333761633131393465376433663233666331636437333137323263663438',1493260754,X'646667686A'),
	(23,X'3138383239323931393139403136332E636F6D',X'3138373131313130303030',X'3562626136353630383062353239376161386232653466613637646165346330',1493261118,X'796F796F'),
	(24,X'7863764073642E76626E6D',X'3138373131313130303030',X'3533363235326263633230646635393634303061303465613730303261393139',1493261799,X'6667686A6B'),
	(25,X'6D6F726E696E674071712E636F6D',X'3138373131313130303030',X'6563646365613732363535333866646236333838323434626262636239623730',1493343074,X'6D6F726E696E67'),
	(26,X'71797A314071712E636F6D',X'3138373131313130303030',X'6563646365613732363535333866646236333838323434626262636239623730',1493349218,X'71797A31'),
	(27,X'7A70403136332E636F6D',X'3138373131313130303030',X'3235643535616432383361613430306166343634633736643731336330376164',1493350069,X'7A70707070'),
	(28,X'676667406A6A2E636F6D',X'3138373131313130303030',X'3665376233613131653965623765633035343432303262376263303531643462',1493350652,X'6368656E67'),
	(29,X'7171314071712E636F6D',X'3138373131313130303030',X'6563646365613732363535333866646236333838323434626262636239623730',1493359404,X'717131');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
